###--- CURRENT BASELINE - AUC=0.7534 ---###

# BUILD MODEL STEP-BY-STEP AND LEARN FEATURES
# TRY TO USE INVERSE SORTING OF FILTER SIZE: LARGE -> SMALL
# ANDREA: THIS IS WAY MORE EFFICIENT THAN THE OPPOSITE!

# standard library imports
from __future__ import absolute_import, division, print_function

try:
    import tensorflow as tf
    from tensorflow.keras import datasets, layers, models
    from keras.layers import LeakyReLU
    from keras.utils.vis_utils import plot_model
    from sklearn.model_selection import train_test_split
except:
    tensorflow=False

from os import path
import sys
# standard numerical library imports
import numpy as np
import h5py
import math

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

# ROOT-related imports
try:
    import ROOT
except:
    print('please install ROOT in order to produce maps')
    ROOT=False
import helpers

################################### SETTINGS ###################################

# data/training setting handles
# same number of S and B events?
# N.B. applicable if new images are dumped
balanced_data = False
# fraction of test/validation samples
test_frac = 0.30
###val_frac, test_frac = 0.1, 0.15
# n. of epochs
nEpochs=100
# size of batch, i.e. number of training examples utilized in one iteration
batchSize=600
# default learning rate for 'adam'
# optimizers is 1e-3
# -> adam with learning rate of 1.2e-3 seems ~optimal
learningRate=1.2e-3

# FIXME: old switches, check whether useful
# optional network architecture parameters
###dense_sizes = [50]
###pool_sizes = 2
# regularization
###conv_dropouts=0

################################################################################

# plot some data inputs to have a look?
plotData = False#True
nPlots = 900

# overwrite hdf5 file with new data?
# applicable only if the input hdf5
# file is already available
writeDataToH5 = False#True

# check whether data is already available
# in a hdf5 file and, if yes, do not clobber it
# unless writeDataToH5 = True
hdf5Name = 'dataset.h5'
dataIsThere = path.exists(hdf5Name)

if (dataIsThere and not writeDataToH5):
    print('WARNING: '+hdf5Name+' is available and I am going to take images/labels from there!')
    # load dataset
    h5f = h5py.File(hdf5Name,'r')
    images = h5f['images'][:]
    y = h5f['labels'][:]
else:
    # load data
    S_inputRootFiles='/eos/atlas/user/a/asciandr/Hbb/jetimages/ggH.root'
    B_inputRootFiles='/eos/atlas/user/a/asciandr/Hbb/jetimages/QCD_AUGMENTED_Info_evttree.root'
    # signal ntuple
    print('Opening signal ROOT file...')
    f_sig = ROOT.TFile(S_inputRootFiles)
    # signal maps
    print('Building signal maps...')
    S_boh=[]
    for i,e in enumerate(f_sig.evttree):
        map_i,fji = helpers.produce_map(e,i)
        if (map_i.Integral()==0):
            continue
        S_boh.append(helpers.dumb_bin(map_i))
    # background ntuple
    print('Opening background ROOT file...')
    f_bkg = ROOT.TFile(B_inputRootFiles)
    # background maps
    print('Building background maps...')
    B_boh=[]
    for i,e in enumerate(f_bkg.evttree):
        map_i,fji = helpers.produce_map(e,i)
        if (map_i.Integral()==0):
            continue
        B_boh.append(helpers.dumb_bin(map_i))
    
    # balanced data size?
    S_boh_new=[]
    B_boh_new=[]
    if(balanced_data and len(S_boh)!=len(B_boh)):
        print('Unbalanced dataset and you asked me to balance it!')
        n_max = len(S_boh)
        if (len(S_boh)>len(B_boh)):
            n_max = len(B_boh)
        print('Number of S and B events:\t%d',n_max)
        S_boh_new = sample(S_boh,n_max) 
        B_boh_new = sample(B_boh,n_max) 
    else:
        S_boh_new = S_boh
        B_boh_new = B_boh
    
    # signal images
    S_pre_images = np.asarray([x for x in S_boh_new])
    # background images
    B_pre_images = np.asarray([x for x in B_boh_new])
    
    S_images = np.expand_dims(S_pre_images, axis=1)
    B_images = np.expand_dims(B_pre_images, axis=1)
    
    # concatenate S and B images
    images = np.concatenate((S_images, B_images), axis=0)
    
    # S or B?
    y = np.array([-1 for x in images])
    for idx,image in enumerate(images):
        if(idx<len(S_boh_new)):
            y[idx]=1
        else:
            y[idx]=0
    
    # save images and labels into hdf5 file
    if (writeDataToH5):
        print('Going to save dataset in '+hdf5Name+'...')
        h5f = h5py.File(hdf5Name, 'w')
        h5f.create_dataset('images', data=images)
        h5f.create_dataset('labels', data=y)
        h5f.close()

# data stored for EF-like CNN have 
# shape (Nev, 1, Npix, Npix) need to 
# switch to (Nev, Npix, Npix, 1)
print(images.shape)
images = np.squeeze(images, axis=1)
images = np.expand_dims(images, axis=3)
# CHECK: train an input-independent baseline.
# This should perform worse than when you actually plug in your data without zeroing it out.
###images=np.subtract(images,images)
###np.set_printoptions(threshold=sys.maxsize)
###print(images[0])

print(images.shape)
print(y.shape)

print('Done making/retrieving jet images')

# split into train, valid, test
# fixed random_state to reproduce results
# N.B. DOES NOT WORK WITH GPUs, CHARACTERIZED BY MULTIPLE SOURCES OF RANDOMNESS!
x_train, x_valid, y_train, y_valid = train_test_split(images, y, test_size=test_frac, random_state= 1)

print('Done train/val/test split')

if (plotData):
    print('Plotting images')
    for cl in range(2):
        fig = plt.figure(figsize=(10,10))
        myTitle = 'H->bb images' if (cl==0) else 'g->bb images'
        myLabel = 1 if (cl==0) else 0
        fig.suptitle(myTitle, fontsize=20)
        count=0
        for i in range(y_train.shape[0]):
            if (count==nPlots):
                break
            if (y[i]==myLabel):
                if (cl==0):
                    HbbImage = x_train[i]/nPlots if (count==0) else HbbImage+x_train[i]/nPlots
                else:
                    gbbImage = x_train[i]/nPlots if (count==0) else gbbImage+x_train[i]/nPlots
                plt.subplot(int(math.sqrt(nPlots)),int(math.sqrt(nPlots)),count+1)
                plt.xticks([])
                plt.yticks([])
                plt.grid(False)
                plt.imshow(np.squeeze(x_train[i], axis=2), cmap='jet', vmin=0, vmax=0.01)
                count=count+1
        plt.show()
    # summed images
    np.set_printoptions(threshold=sys.maxsize)
    print('Hbb MINUS gbb Image:')
    plt.imshow(np.squeeze(np.subtract(HbbImage,gbbImage), axis=2), cmap='jet', vmin=-0.01, vmax=0.01)
    fig.suptitle('Summed g->bb images', fontsize=20)
    plt.show()
    print('Hbb Image:')
    ###print(HbbImage)
    plt.imshow(np.squeeze(HbbImage, axis=2), cmap='jet', vmin=0, vmax=0.01)
    fig.suptitle('Summed H->bb images', fontsize=20)
    plt.show()
    print('gbb Image:')
    ###print(gbbImage)
    plt.imshow(np.squeeze(gbbImage, axis=2), cmap='jet', vmin=0, vmax=0.01)
    fig.suptitle('Summed g->bb images', fontsize=20)
    plt.show()

print('Finished pre-processing. Building model, step-by-step.')

###--- BUILD MODEL STEP-BY-STEP ---###
model = models.Sequential()
model.add(layers.Conv2D(4, (10, 10), activation='relu', input_shape=(50, 50, 1)))

# ANDREA: test more 10x10 filters to try
# extract features of smaller track "clusters"
# -> does not help, on the contrary
###model.add(layers.Conv2D(9, (10, 10), activation='relu', input_shape=(50, 50, 1)))
###model.add(layers.MaxPooling2D((2, 2)))
###model.add(layers.AveragePooling2D((2, 2)))
model.add(layers.Conv2D(4, (5, 5), activation='relu'))
model.add(layers.Conv2D(4, (3, 3), activation='relu'))
###model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(4, (2, 2), activation='relu'))
###model.add(layers.MaxPooling2D((2, 2)))
###model.add(layers.LeakyReLU(alpha=0.01))

# do not need to input shape
# for flattening when starting 
# with convolution+pooling steps!
model.add(layers.Flatten())
###model.add(layers.LeakyReLU(alpha=0.01))

# ANDREA: try to have 2 fully connected layers
# -> does not seem to help!
###model.add(layers.Dense(4624, activation='relu'))
###model.add(layers.Dense(40, activation='relu'))

# ANDREA: test sigmoid for binary NN classifier
# see https://www.geeksforgeeks.org/activation-functions-neural-networks/
# -> significantly worse performance (AUC~0.69)
###model.add(layers.Dense(2, activation='sigmoid'))
model.add(layers.Dense(2, activation='softmax'))

print('Model summary:')

model.summary()
# lxplus singularities do not have pydot...
try:
    plot_model(model, to_file='Basic_model.png', show_shapes=True, show_layer_names=True)
except:
    print('WARNING Could not save model summary structure in image format')

# early stopping to regularize model
# https://keras.io/api/callbacks/early_stopping/
callback = tf.keras.callbacks.EarlyStopping(
    monitor="val_loss",
    min_delta=0,
    patience=8,
    verbose=10,
    mode="auto",
    baseline=None,
    restore_best_weights=True,
)

# set adam learning rate
# https://twitter.com/karpathy/status/801621764144971776?lang=en
# N.B. with python3.7 keras libraries the argeument changed from learning_rate to lr
try:
    opt = tf.keras.optimizers.Adam(lr=learningRate)
except:
    opt = tf.keras.optimizers.Adam(learning_rate=learningRate)

print('Compiling model...')

# ANDREA: test different optimizers,
# even though adam seems to be best
# suited for CNN tasks: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7407771/
# -> AND, IN FACT, IT SEEMS SO!
###model.compile(optimizer='adam',
# adam optimizer, with given learning rate
model.compile(optimizer=opt,
###model.compile(optimizer='sgd',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

print('Starting model training...')

history = model.fit(x_train, y_train, 
                    validation_data=(x_valid,y_valid),
                    batch_size=batchSize,
                    epochs=nEpochs,
                    callbacks=[callback],
                    verbose=2)

print('Save output model & weights')

model.save_weights('Basic_weights.h5')
model.save('Basic_model.h5')

helpers.plot_history([('baseline', history)],'loss')

valid_loss, valid_acc = model.evaluate(x_valid,y_valid)
print(valid_acc)

# get ROC curve if we have sklearn
if roc_curve:
    print('Build ROC curves...')
    test_preds = model.predict(x_valid, batch_size=1000)
    print(y_valid.shape)
    print(test_preds[:,1].shape)
    cnn_fp, cnn_tp, threshs = roc_curve(y_valid, test_preds[:,1])

    # get area under the ROC curve
    auc = roc_auc_score(y_valid, test_preds[:,1])
    print()
    print('CNN AUC:', auc)
    print()

    # make ROC curve plot if we have matplotlib
    if plt:

        # some nicer plot settings 
        plt.rcParams['figure.figsize'] = (4,4)
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['figure.autolayout'] = True

        # plot the ROC curves
        plt.plot(cnn_tp, 1-cnn_fp, '-', color='black', label='CNN')

        # axes labels
        plt.xlabel('H->bb Efficiency')
        plt.ylabel('g->bb Rejection')

        # axes limits
        plt.xlim(0, 1)
        plt.ylim(0, 1)

        # make legend and show plot
        plt.legend(loc='lower left', frameon=False)
        plt.show()

print('DONE.')
