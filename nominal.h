//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Mar 25 12:10:48 2019 by ROOT version 6.16/00
// from TTree evttree/evttree
// found on file: user.asciandr.17456028._000002.evttree.root
//////////////////////////////////////////////////////////

#ifndef nominal_h
#define nominal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TString.h"
#include "vector"
#include "vector"

class nominal {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           lumiBlock;
   Float_t         totweight;
   ULong64_t       tot_events;
   Float_t         averagePU;
   Float_t         eventPU;
   Int_t           mcFlag;
   Int_t           mcProcess;
   TString         *jetCalConf;
   TString         *fatJetCalConf;
   Bool_t          HLT_j420_a10t_lcw_jes_35smcINF_L1SC111;
   Bool_t          HLT_j420_a10t_lcw_jes_35smcINF_L1J100;
   Bool_t          HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111;
   Bool_t          HLT_2j330_a10t_lcw_jes_35smcINF_L1J100;
   Bool_t          HLT_j360_a10t_lcw_jes_60smcINF_j360_a10t_lcw_jes_L1SC111;
   Bool_t          HLT_j370_a10t_lcw_jes_35smcINF_j370_a10t_lcw_jes_L1SC111;
   Bool_t          HLT_2j330_a10t_lcw_jes_30smcINF_L1J100;
   Bool_t          HLT_j390_a10t_lcw_jes_30smcINF_L1J100;
   Bool_t          HLT_ht1000_L1J100;
   Bool_t          HLT_j420_a10_lcw_L1J100;
   Bool_t          HLT_j420_a10r_L1J100;
   Bool_t          HLT_4j100;
   Bool_t          HLT_j380;
   Bool_t          HLT_j420;
   Bool_t          HLT_j440;
   Bool_t          HLT_j460;
   Bool_t          HLT_j480;
   Bool_t          HLT_j420_a10t_lcw_jes_L1J100;
   Bool_t          HLT_j440_a10t_lcw_jes_L1J100;
   Bool_t          HLT_j460_a10t_lcw_jes_L1J100;
   Bool_t          HLT_j480_a10_lcw_sub_L1J100;
   Bool_t          HLT_j420_a10_lcw_subjes_L1J100;
   Bool_t          HLT_j440_a10_lcw_subjes_L1J100;
   Bool_t          HLT_j460_a10_lcw_subjes_L1J100;
   Bool_t          HLT_j480_a10_lcw_subjes_L1J100;
   Bool_t          HLT_j460_a10r_L1SC111;
   Bool_t          HLT_j460_a10r_L1J100;
   Bool_t          HLT_j460_a10_lcw_subjes_L1SC111;
   Bool_t          HLT_j460_a10t_lcw_jes_L1SC111;
   Bool_t          HLT_j480_a10t_lcw_jes_L1J100;
   Bool_t          HLT_j440_a10t_lcw_jes_40smcINF_L1J100;
   Bool_t          HLT_2j330_a10t_lcw_jes_40smcINF_L1J100;
   Bool_t          HLT_j420_a10t_lcw_jes_40smcINF_L1J100;
   Float_t         eventMCWeight;
   Float_t         truthVtxX;
   Float_t         truthVtxY;
   Float_t         truthVtxZ;
   Float_t         pVtxZ;
   Float_t         MET;
   Float_t         phiMET;
   vector<int>     *bosonPdgId;
   vector<int>     *bosonStatus;
   vector<int>     *bosonBarcode;
   vector<float>   *bosonPt;
   vector<float>   *bosonPx;
   vector<float>   *bosonPy;
   vector<float>   *bosonPz;
   vector<float>   *bosonE;
   vector<float>   *bosonM;
   vector<int>     *bosonChildPdgId;
   vector<int>     *partonPdgId;
   vector<int>     *partonStatus;
   vector<int>     *partonBarcode;
   vector<float>   *partonPt;
   vector<float>   *partonPx;
   vector<float>   *partonPy;
   vector<float>   *partonPz;
   vector<float>   *partonE;
   vector<float>   *partonM;
   vector<int>     *partonParentPdgId;
   vector<int>     *partonParentBarcode;
   vector<int>     *leptonPdgId;
   vector<int>     *leptonStatus;
   vector<int>     *leptonBarcode;
   vector<float>   *leptonPt;
   vector<float>   *leptonPx;
   vector<float>   *leptonPy;
   vector<float>   *leptonPz;
   vector<float>   *leptonE;
   vector<float>   *leptonM;
   vector<int>     *leptonParentPdgId;
   vector<int>     *leptonParentBarcode;
   vector<int>     *bHadronPdgId;
   vector<int>     *bHadronBarcode;
   vector<float>   *bHadronPt;
   vector<float>   *bHadronPx;
   vector<float>   *bHadronPy;
   vector<float>   *bHadronPz;
   vector<float>   *bHadronE;
   vector<float>   *bHadronM;
   vector<int>     *bHadronNSec;
   vector<int>     *bHadronNTer;
   vector<int>     *bHadronNChSec;
   vector<int>     *bHadronNChTer;
   vector<int>     *bHadronNLepSec;
   vector<int>     *bHadronNLepTer;
   vector<int>     *bHadronNK0s;
   Int_t           tagCode;
   Float_t         tagDiJetMH;
   Float_t         tagDJetPtH;
   Float_t         tagDiJetPt1;
   Float_t         tagDiJetPt2;
   vector<float>   *jetPt;
   vector<float>   *jetPx;
   vector<float>   *jetPy;
   vector<float>   *jetPz;
   vector<float>   *jetE;
   vector<float>   *jetM;
   vector<float>   *jetQLoosek03;
   vector<float>   *jetQLoosek05;
   vector<float>   *jetQLoosek07;
   vector<float>   *jetQLooseVtxk03;
   vector<float>   *jetQLooseVtxk05;
   vector<float>   *jetQLooseVtxk07;
   vector<int>     *jetNTracks;
   vector<int>     *jetNLooseTracks;
   vector<int>     *jetNLooseVtxTracks;
   vector<float>   *jetJVT;
   vector<int>     *jetFlavour;
   vector<float>   *jetMV2c10;
   vector<float>   *jetMV2c10mu;
   vector<float>   *jetMV2c10rnn;
   TString         *jetHybBEffTagger;
   vector<int>     *jetHybBEff_60;
   vector<int>     *jetHybBEff_70;
   vector<int>     *jetHybBEff_77;
   vector<int>     *jetHybBEff_85;
   vector<float>   *jetSFHybBEff_60;
   vector<float>   *jetSFHybBEff_70;
   vector<float>   *jetSFHybBEff_77;
   vector<float>   *jetSFHybBEff_85;
   vector<float>   *jetIP3Dpu;
   vector<float>   *jetIP3Dpc;
   vector<float>   *jetIP3Dpb;
   vector<float>   *jetIPRNN;
   vector<float>   *jetIPRNNpb;
   vector<float>   *jetIPRNNpc;
   vector<float>   *jetIPRNNpu;
   vector<float>   *jetJFEfrc;
   vector<float>   *jetJFMass;
   vector<float>   *jetJFdEta;
   vector<float>   *jetJFdPhi;
   vector<int>     *jetJFNVtx;
   vector<int>     *jetJFNTrkAtVtx;
   vector<int>     *jetJFNSingleTrk;
   vector<float>   *jetJFSig3D;
   vector<float>   *jetSV1Efrc;
   vector<float>   *jetSV1Mass;
   vector<int>     *jetSV1NTrkAtVtx;
   vector<float>   *jetSV1Sig3D;
   vector<float>   *jetJFPVtxX;
   vector<float>   *jetJFPVtxY;
   vector<float>   *jetJFPVtxZ;
   vector<float>   *jetJFPVtxXErr;
   vector<float>   *jetJFPVtxYErr;
   vector<float>   *jetJFPVtxZErr;
   vector<float>   *jetJFPhi;
   vector<float>   *jetJFPhiErr;
   vector<float>   *jetJFTheta;
   vector<float>   *jetJFThetaErr;
   vector<float>   *jetJFSecVtxChi2;
   vector<int>     *jetJFSecVtxNdf;
   vector<int>     *jetJFSecVtxNTrk;
   vector<float>   *jetJFSecVtxX;
   vector<float>   *jetJFSecVtxY;
   vector<float>   *jetJFSecVtxZ;
   vector<float>   *jetJFSecVtxXErr;
   vector<float>   *jetJFSecVtxYErr;
   vector<float>   *jetJFSecVtxZErr;
   vector<float>   *jetJFSecVtxL3D;
   vector<float>   *jetJFSecVtxL3DErr;
   vector<float>   *jetJFSecVtxLxy;
   vector<float>   *jetJFSecVtxLxyErr;
   vector<float>   *jetJFSecVtxMass;
   vector<float>   *jetJFSecVtxE;
   vector<float>   *jetJFSecVtxEfrac;
   vector<float>   *jetJFTerVtxChi2;
   vector<int>     *jetJFTerVtxNdf;
   vector<int>     *jetJFTerVtxNTrk;
   vector<float>   *jetJFTerVtxX;
   vector<float>   *jetJFTerVtxY;
   vector<float>   *jetJFTerVtxZ;
   vector<float>   *jetJFTerVtxXErr;
   vector<float>   *jetJFTerVtxYErr;
   vector<float>   *jetJFTerVtxZErr;
   vector<float>   *jetJFTerVtxL3D;
   vector<float>   *jetJFTerVtxL3DErr;
   vector<float>   *jetJFTerVtxLxy;
   vector<float>   *jetJFTerVtxLxyErr;
   vector<float>   *jetJFTerVtxMass;
   vector<float>   *jetJFTerVtxE;
   vector<float>   *jetJFTerVtxEfrac;
   vector<float>   *muonPt;
   vector<float>   *muonPx;
   vector<float>   *muonPy;
   vector<float>   *muonPz;
   vector<float>   *muonCharge;
   vector<float>   *muonSMT;
   vector<float>   *muonD0;
   vector<float>   *muonPtRel;
   vector<float>   *electronPt;
   vector<float>   *electronPx;
   vector<float>   *electronPy;
   vector<float>   *electronPz;
   vector<float>   *electronCharge;
   Float_t         diMuonMass;
   Float_t         diElectronMass;
   Float_t         muonElectronMass;
   vector<float>   *fatJetPt;
   vector<float>   *fatJetPx;
   vector<float>   *fatJetPy;
   vector<float>   *fatJetPz;
   vector<float>   *fatJetE;
   vector<float>   *fatJetM;
   vector<float>   *fatJetXbbM;
   vector<float>   *fatJetPtRecoM;
   vector<int>     *fatJetNTracks;
   vector<float>   *fatJetD2;
   vector<int>     *fatJetNTrkJets;
   vector<int>     *fatJetNVRJets;
   vector<int>     *fatJetNGhostTop;
   vector<int>     *fatJetNGhostW;
   vector<int>     *fatJetNGhostZ;
   vector<int>     *fatJetNGhostH;
   vector<float>   *fatJetDiTrkJetMass;
   vector<float>   *fatJetDiTrkJetPt;
   vector<float>   *fatJetDiVRJetMass;
   vector<float>   *fatJetDiVRJetPt;
   vector<float>   *trackJetPt;
   vector<float>   *trackJetPx;
   vector<float>   *trackJetPy;
   vector<float>   *trackJetPz;
   vector<float>   *trackJetE;
   vector<float>   *trackJetM;
   vector<int>     *trackJetNTracks;
   vector<int>     *trackJetIdFatJet;
   vector<int>     *trackJetFlavour;
   vector<float>   *trackJetMV2c10;
   vector<float>   *trackJetMV2c10mu;
   vector<float>   *trackJetMV2c10rnn;
   TString         *trackJetBTagger;
   vector<int>     *trackJetHybBEff_60;
   vector<int>     *trackJetHybBEff_70;
   vector<int>     *trackJetHybBEff_77;
   vector<int>     *trackJetHybBEff_85;
   vector<float>   *trackJetSFHybBEff_60;
   vector<float>   *trackJetSFHybBEff_70;
   vector<float>   *trackJetSFHybBEff_77;
   vector<float>   *trackJetSFHybBEff_85;
   vector<float>   *trackJetIP3Dpu;
   vector<float>   *trackJetIP3Dpc;
   vector<float>   *trackJetIP3Dpb;
   vector<float>   *trackJetRNNIP;
   vector<float>   *trackJetRNNIPpu;
   vector<float>   *trackJetRNNIPpc;
   vector<float>   *trackJetRNNIPpb;
   vector<float>   *trackJetJFEfrc;
   vector<float>   *trackJetJFMass;
   vector<float>   *trackJetJFdEta;
   vector<float>   *trackJetJFdPhi;
   vector<int>     *trackJetJFNVtx;
   vector<int>     *trackJetJFNTrkAtVtx;
   vector<int>     *trackJetJFNSingleTrk;
   vector<float>   *trackJetJFSig3D;
   vector<float>   *trackJetJFPhi;
   vector<float>   *trackJetJFPhiErr;
   vector<float>   *trackJetJFTheta;
   vector<float>   *trackJetJFThetaErr;
   vector<float>   *trackJetSV1Efrc;
   vector<float>   *trackJetSV1Mass;
   vector<int>     *trackJetSV1NTrkAtVtx;
   vector<float>   *trackJetSV1Sig3D;
   vector<int>     *trackJetBHadronPdgId;
   vector<int>     *trackJetBHadronBarcode;
   vector<float>   *trackJetBHadronPt;
   vector<float>   *trackJetBHadronPx;
   vector<float>   *trackJetBHadronPy;
   vector<float>   *trackJetBHadronPz;
   vector<float>   *trackJetBHadronE;
   vector<float>   *trackJetBHadronM;
   vector<float>   *trackJetBHadronProdVtxX;
   vector<float>   *trackJetBHadronProdVtxY;
   vector<float>   *trackJetBHadronProdVtxZ;
   vector<float>   *trackJetBHadronDecVtxX;
   vector<float>   *trackJetBHadronDecVtxY;
   vector<float>   *trackJetBHadronDecVtxZ;
   vector<float>   *trackJetBCHadronDecVtxX;
   vector<float>   *trackJetBCHadronDecVtxY;
   vector<float>   *trackJetBCHadronDecVtxZ;
   vector<int>     *trackJetBHadronNSec;
   vector<int>     *trackJetBHadronNTer;
   vector<int>     *trackJetBHadronNChSec;
   vector<int>     *trackJetBHadronNChTer;
   vector<int>     *trackJetBHadronNLepSec;
   vector<int>     *trackJetBHadronNLepTer;
   vector<int>     *trackJetBHadronNK0s;
   vector<float>   *trackJetBHadronMassDec;
   vector<float>   *trackJetBHadronMassSec;
   vector<float>   *trackJetBHadronMassTer;
   vector<float>   *vrJetPt;
   vector<float>   *vrJetPx;
   vector<float>   *vrJetPy;
   vector<float>   *vrJetPz;
   vector<float>   *vrJetE;
   vector<float>   *vrJetM;
   vector<int>     *vrJetNTracks;
   vector<int>     *vrJetIdFatJet;
   vector<float>   *vrJetMV2c10;
   vector<float>   *vrJetMV2c10mu;
   vector<float>   *vrJetMV2c10rnn;
   TString         *vrJetBTagger;
   vector<int>     *vrJetHybBEff_60;
   vector<int>     *vrJetHybBEff_70;
   vector<int>     *vrJetHybBEff_77;
   vector<int>     *vrJetHybBEff_85;
   vector<float>   *vrJetSFHybBEff_60;
   vector<float>   *vrJetSFHybBEff_70;
   vector<float>   *vrJetSFHybBEff_77;
   vector<float>   *vrJetSFHybBEff_85;
   vector<float>   *vrJetIP3Dpu;
   vector<float>   *vrJetIP3Dpc;
   vector<float>   *vrJetIP3Dpb;
   vector<float>   *vrJetRNNIP;
   vector<float>   *vrJetRNNIPpu;
   vector<float>   *vrJetRNNIPpc;
   vector<float>   *vrJetRNNIPpb;
   vector<int>     *vrJetBHadronPdgId;
   vector<int>     *vrJetBHadronBarcode;
   vector<float>   *vrJetBHadronPt;
   vector<float>   *vrJetBHadronPx;
   vector<float>   *vrJetBHadronPy;
   vector<float>   *vrJetBHadronPz;
   vector<float>   *vrJetBHadronE;
   vector<float>   *vrJetBHadronM;
   vector<float>   *vrJetBHadronProdVtxX;
   vector<float>   *vrJetBHadronProdVtxY;
   vector<float>   *vrJetBHadronProdVtxZ;
   vector<float>   *vrJetBHadronDecVtxX;
   vector<float>   *vrJetBHadronDecVtxY;
   vector<float>   *vrJetBHadronDecVtxZ;
   vector<float>   *vrJetBCHadronDecVtxX;
   vector<float>   *vrJetBCHadronDecVtxY;
   vector<float>   *vrJetBCHadronDecVtxZ;
   vector<int>     *vrJetBHadronNSec;
   vector<int>     *vrJetBHadronNTer;
   vector<int>     *vrJetBHadronNChSec;
   vector<int>     *vrJetBHadronNChTer;
   vector<int>     *vrJetBHadronNLepSec;
   vector<int>     *vrJetBHadronNLepTer;
   vector<int>     *vrJetBHadronNK0s;
   vector<float>   *vrJetBHadronMassDec;
   vector<float>   *vrJetBHadronMassSec;
   vector<float>   *vrJetBHadronMassTer;
   vector<float>   *trackPx;
   vector<float>   *trackPy;
   vector<float>   *trackPz;
   vector<float>   *trackPt;
   vector<float>   *trackPhi;
   vector<float>   *trackTheta;
   vector<float>   *trackQOverP;
   vector<float>   *trackEta;
   vector<int>     *trackNPixelLayers;
   vector<int>     *trackNPixelHits;
   vector<int>     *trackNPixelHoles;
   vector<int>     *trackNIBLHits;
   vector<int>     *trackNBLHits;
   vector<int>     *trackNIBLSharedHits;
   vector<int>     *trackNIBLSplitHits;
   vector<int>     *trackNBLSharedHits;
   vector<int>     *trackNBLSplitHits;
   vector<int>     *trackNIBLExpectedHits;
   vector<int>     *trackNBLExpectedHits;
   vector<int>     *trackNSCTHits;
   vector<int>     *trackNSCTHoles;
   vector<int>     *trackNSCTSharedHits;
   vector<int>     *trackNTRTHits;
   vector<float>   *trackD0;
   vector<float>   *trackZ0;
   vector<float>   *trackD0Err;
   vector<float>   *trackZ0Err;
   vector<int>     *trackTruthPdgId;
   vector<int>     *trackTruthBarcode;
   vector<int>     *trackTruthParentFlavour;
   vector<int>     *trackTruthParentBarcode;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_totweight;   //!
   TBranch        *b_tot_events;   //!
   TBranch        *b_averagePU;   //!
   TBranch        *b_eventPU;   //!
   TBranch        *b_mcFlag;   //!
   TBranch        *b_mcProcess;   //!
   TBranch        *b_jetCalConf;   //!
   TBranch        *b_fatJetCalConf;   //!
   TBranch        *b_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111;   //!
   TBranch        *b_HLT_j420_a10t_lcw_jes_35smcINF_L1J100;   //!
   TBranch        *b_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111;   //!
   TBranch        *b_HLT_2j330_a10t_lcw_jes_35smcINF_L1J100;   //!
   TBranch        *b_HLT_j360_a10t_lcw_jes_60smcINF_j360_a10t_lcw_jes_L1SC111;   //!
   TBranch        *b_HLT_j370_a10t_lcw_jes_35smcINF_j370_a10t_lcw_jes_L1SC111;   //!
   TBranch        *b_HLT_2j330_a10t_lcw_jes_30smcINF_L1J100;   //!
   TBranch        *b_HLT_j390_a10t_lcw_jes_30smcINF_L1J100;   //!
   TBranch        *b_HLT_ht1000_L1J100;   //!
   TBranch        *b_HLT_j420_a10_lcw_L1J100;   //!
   TBranch        *b_HLT_j420_a10r_L1J100;   //!
   TBranch        *b_HLT_4j100;   //!
   TBranch        *b_HLT_j380;   //!
   TBranch        *b_HLT_j420;   //!
   TBranch        *b_HLT_j440;   //!
   TBranch        *b_HLT_j460;   //!
   TBranch        *b_HLT_j480;   //!
   TBranch        *b_HLT_j420_a10t_lcw_jes_L1J100;   //!
   TBranch        *b_HLT_j440_a10t_lcw_jes_L1J100;   //!
   TBranch        *b_HLT_j460_a10t_lcw_jes_L1J100;   //!
   TBranch        *b_HLT_j480_a10_lcw_sub_L1J100;   //!
   TBranch        *b_HLT_j420_a10_lcw_subjes_L1J100;   //!
   TBranch        *b_HLT_j440_a10_lcw_subjes_L1J100;   //!
   TBranch        *b_HLT_j460_a10_lcw_subjes_L1J100;   //!
   TBranch        *b_HLT_j480_a10_lcw_subjes_L1J100;   //!
   TBranch        *b_HLT_j460_a10r_L1SC111;   //!
   TBranch        *b_HLT_j460_a10r_L1J100;   //!
   TBranch        *b_HLT_j460_a10_lcw_subjes_L1SC111;   //!
   TBranch        *b_HLT_j460_a10t_lcw_jes_L1SC111;   //!
   TBranch        *b_HLT_j480_a10t_lcw_jes_L1J100;   //!
   TBranch        *b_HLT_j440_a10t_lcw_jes_40smcINF_L1J100;   //!
   TBranch        *b_HLT_2j330_a10t_lcw_jes_40smcINF_L1J100;   //!
   TBranch        *b_HLT_j420_a10t_lcw_jes_40smcINF_L1J100;   //!
   TBranch        *b_eventMCWeight;   //!
   TBranch        *b_truthVtxX;   //!
   TBranch        *b_truthVtxY;   //!
   TBranch        *b_truthVtxZ;   //!
   TBranch        *b_pVtxZ;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_phiMET;   //!
   TBranch        *b_bosonPdgId;   //!
   TBranch        *b_bosonStatus;   //!
   TBranch        *b_bosonBarcode;   //!
   TBranch        *b_bosonPt;   //!
   TBranch        *b_bosonPx;   //!
   TBranch        *b_bosonPy;   //!
   TBranch        *b_bosonPz;   //!
   TBranch        *b_bosonE;   //!
   TBranch        *b_bosonM;   //!
   TBranch        *b_bosonChildPdgId;   //!
   TBranch        *b_partonPdgId;   //!
   TBranch        *b_partonStatus;   //!
   TBranch        *b_partonBarcode;   //!
   TBranch        *b_partonPt;   //!
   TBranch        *b_partonPx;   //!
   TBranch        *b_partonPy;   //!
   TBranch        *b_partonPz;   //!
   TBranch        *b_partonE;   //!
   TBranch        *b_partonM;   //!
   TBranch        *b_partonParentPdgId;   //!
   TBranch        *b_partonParentBarcode;   //!
   TBranch        *b_leptonPdgId;   //!
   TBranch        *b_leptonStatus;   //!
   TBranch        *b_leptonBarcode;   //!
   TBranch        *b_leptonPt;   //!
   TBranch        *b_leptonPx;   //!
   TBranch        *b_leptonPy;   //!
   TBranch        *b_leptonPz;   //!
   TBranch        *b_leptonE;   //!
   TBranch        *b_leptonM;   //!
   TBranch        *b_leptonParentPdgId;   //!
   TBranch        *b_leptonParentBarcode;   //!
   TBranch        *b_bHadronPdgId;   //!
   TBranch        *b_bHadronBarcode;   //!
   TBranch        *b_bHadronPt;   //!
   TBranch        *b_bHadronPx;   //!
   TBranch        *b_bHadronPy;   //!
   TBranch        *b_bHadronPz;   //!
   TBranch        *b_bHadronE;   //!
   TBranch        *b_bHadronM;   //!
   TBranch        *b_bHadronNSec;   //!
   TBranch        *b_bHadronNTer;   //!
   TBranch        *b_bHadronNChSec;   //!
   TBranch        *b_bHadronNChTer;   //!
   TBranch        *b_bHadronNLepSec;   //!
   TBranch        *b_bHadronNLepTer;   //!
   TBranch        *b_bHadronNK0s;   //!
   TBranch        *b_tagCode;   //!
   TBranch        *b_tagDiJetMH;   //!
   TBranch        *b_tagDJetPtH;   //!
   TBranch        *b_tagDiJetPt1;   //!
   TBranch        *b_tagDiJetPt2;   //!
   TBranch        *b_jetPt;   //!
   TBranch        *b_jetPx;   //!
   TBranch        *b_jetPy;   //!
   TBranch        *b_jetPz;   //!
   TBranch        *b_jetE;   //!
   TBranch        *b_jetM;   //!
   TBranch        *b_jetQLoosek03;   //!
   TBranch        *b_jetQLoosek05;   //!
   TBranch        *b_jetQLoosek07;   //!
   TBranch        *b_jetQLooseVtxk03;   //!
   TBranch        *b_jetQLooseVtxk05;   //!
   TBranch        *b_jetQLooseVtxk07;   //!
   TBranch        *b_jetNTracks;   //!
   TBranch        *b_jetNLooseTracks;   //!
   TBranch        *b_jetNLooseVtxTracks;   //!
   TBranch        *b_jetJVT;   //!
   TBranch        *b_jetFlavour;   //!
   TBranch        *b_jetMV2c10;   //!
   TBranch        *b_jetMV2c10mu;   //!
   TBranch        *b_jetMV2c10rnn;   //!
   TBranch        *b_jetHybBEffTagger;   //!
   TBranch        *b_jetHybBEff_60;   //!
   TBranch        *b_jetHybBEff_70;   //!
   TBranch        *b_jetHybBEff_77;   //!
   TBranch        *b_jetHybBEff_85;   //!
   TBranch        *b_jetSFHybBEff_60;   //!
   TBranch        *b_jetSFHybBEff_70;   //!
   TBranch        *b_jetSFHybBEff_77;   //!
   TBranch        *b_jetSFHybBEff_85;   //!
   TBranch        *b_jetIP3Dpu;   //!
   TBranch        *b_jetIP3Dpc;   //!
   TBranch        *b_jetIP3Dpb;   //!
   TBranch        *b_jetIPRNN;   //!
   TBranch        *b_jetIPRNNpb;   //!
   TBranch        *b_jetIPRNNpc;   //!
   TBranch        *b_jetIPRNNpu;   //!
   TBranch        *b_jetJFEfrc;   //!
   TBranch        *b_jetJFMass;   //!
   TBranch        *b_jetJFdEta;   //!
   TBranch        *b_jetJFdPhi;   //!
   TBranch        *b_jetJFNVtx;   //!
   TBranch        *b_jetJFNTrkAtVtx;   //!
   TBranch        *b_jetJFNSingleTrk;   //!
   TBranch        *b_jetJFSig3D;   //!
   TBranch        *b_jetSV1Efrc;   //!
   TBranch        *b_jetSV1Mass;   //!
   TBranch        *b_jetSV1NTrkAtVtx;   //!
   TBranch        *b_jetSV1Sig3D;   //!
   TBranch        *b_jetJFPVtxX;   //!
   TBranch        *b_jetJFPVtxY;   //!
   TBranch        *b_jetJFPVtxZ;   //!
   TBranch        *b_jetJFPVtxXErr;   //!
   TBranch        *b_jetJFPVtxYErr;   //!
   TBranch        *b_jetJFPVtxZErr;   //!
   TBranch        *b_jetJFPhi;   //!
   TBranch        *b_jetJFPhiErr;   //!
   TBranch        *b_jetJFTheta;   //!
   TBranch        *b_jetJFThetaErr;   //!
   TBranch        *b_jetJFSecVtxChi2;   //!
   TBranch        *b_jetJFSecVtxNdf;   //!
   TBranch        *b_jetJFSecVtxNTrk;   //!
   TBranch        *b_jetJFSecVtxX;   //!
   TBranch        *b_jetJFSecVtxY;   //!
   TBranch        *b_jetJFSecVtxZ;   //!
   TBranch        *b_jetJFSecVtxXErr;   //!
   TBranch        *b_jetJFSecVtxYErr;   //!
   TBranch        *b_jetJFSecVtxZErr;   //!
   TBranch        *b_jetJFSecVtxL3D;   //!
   TBranch        *b_jetJFSecVtxL3DErr;   //!
   TBranch        *b_jetJFSecVtxLxy;   //!
   TBranch        *b_jetJFSecVtxLxyErr;   //!
   TBranch        *b_jetJFSecVtxMass;   //!
   TBranch        *b_jetJFSecVtxE;   //!
   TBranch        *b_jetJFSecVtxEfrac;   //!
   TBranch        *b_jetJFTerVtxChi2;   //!
   TBranch        *b_jetJFTerVtxNdf;   //!
   TBranch        *b_jetJFTerVtxNTrk;   //!
   TBranch        *b_jetJFTerVtxX;   //!
   TBranch        *b_jetJFTerVtxY;   //!
   TBranch        *b_jetJFTerVtxZ;   //!
   TBranch        *b_jetJFTerVtxXErr;   //!
   TBranch        *b_jetJFTerVtxYErr;   //!
   TBranch        *b_jetJFTerVtxZErr;   //!
   TBranch        *b_jetJFTerVtxL3D;   //!
   TBranch        *b_jetJFTerVtxL3DErr;   //!
   TBranch        *b_jetJFTerVtxLxy;   //!
   TBranch        *b_jetJFTerVtxLxyErr;   //!
   TBranch        *b_jetJFTerVtxMass;   //!
   TBranch        *b_jetJFTerVtxE;   //!
   TBranch        *b_jetJFTerVtxEfrac;   //!
   TBranch        *b_muonPt;   //!
   TBranch        *b_muonPx;   //!
   TBranch        *b_muonPy;   //!
   TBranch        *b_muonPz;   //!
   TBranch        *b_muonCharge;   //!
   TBranch        *b_muonSMT;   //!
   TBranch        *b_muonD0;   //!
   TBranch        *b_muonPtRel;   //!
   TBranch        *b_electronPt;   //!
   TBranch        *b_electronPx;   //!
   TBranch        *b_electronPy;   //!
   TBranch        *b_electronPz;   //!
   TBranch        *b_electronCharge;   //!
   TBranch        *b_diMuonMass;   //!
   TBranch        *b_diElectronMass;   //!
   TBranch        *b_muonElectronMass;   //!
   TBranch        *b_fatJetPt;   //!
   TBranch        *b_fatJetPx;   //!
   TBranch        *b_fatJetPy;   //!
   TBranch        *b_fatJetPz;   //!
   TBranch        *b_fatJetE;   //!
   TBranch        *b_fatJetM;   //!
   TBranch        *b_fatJetXbbM;   //!
   TBranch        *b_fatJetPtRecoM;   //!
   TBranch        *b_fatJetNTracks;   //!
   TBranch        *b_fatJetD2;   //!
   TBranch        *b_fatJetNTrkJets;   //!
   TBranch        *b_fatJetNVRJets;   //!
   TBranch        *b_fatJetNGhostTop;   //!
   TBranch        *b_fatJetNGhostW;   //!
   TBranch        *b_fatJetNGhostZ;   //!
   TBranch        *b_fatJetNGhostH;   //!
   TBranch        *b_fatJetDiTrkJetMass;   //!
   TBranch        *b_fatJetDiTrkJetPt;   //!
   TBranch        *b_fatJetDiVRJetMass;   //!
   TBranch        *b_fatJetDiVRJetPt;   //!
   TBranch        *b_trackJetPt;   //!
   TBranch        *b_trackJetPx;   //!
   TBranch        *b_trackJetPy;   //!
   TBranch        *b_trackJetPz;   //!
   TBranch        *b_trackJetE;   //!
   TBranch        *b_trackJetM;   //!
   TBranch        *b_trackJetNTracks;   //!
   TBranch        *b_trackJetIdFatJet;   //!
   TBranch        *b_trackJetFlavour;   //!
   TBranch        *b_trackJetMV2c10;   //!
   TBranch        *b_trackJetMV2c10mu;   //!
   TBranch        *b_trackJetMV2c10rnn;   //!
   TBranch        *b_trackJetBTagger;   //!
   TBranch        *b_trackJetHybBEff_60;   //!
   TBranch        *b_trackJetHybBEff_70;   //!
   TBranch        *b_trackJetHybBEff_77;   //!
   TBranch        *b_trackJetHybBEff_85;   //!
   TBranch        *b_trackJetSFHybBEff_60;   //!
   TBranch        *b_trackJetSFHybBEff_70;   //!
   TBranch        *b_trackJetSFHybBEff_77;   //!
   TBranch        *b_trackJetSFHybBEff_85;   //!
   TBranch        *b_trackJetIP3Dpu;   //!
   TBranch        *b_trackJetIP3Dpc;   //!
   TBranch        *b_trackJetIP3Dpb;   //!
   TBranch        *b_trackJetRNNIP;   //!
   TBranch        *b_trackJetRNNIPpu;   //!
   TBranch        *b_trackJetRNNIPpc;   //!
   TBranch        *b_trackJetRNNIPpb;   //!
   TBranch        *b_trackJetJFEfrc;   //!
   TBranch        *b_trackJetJFMass;   //!
   TBranch        *b_trackJetJFdEta;   //!
   TBranch        *b_trackJetJFdPhi;   //!
   TBranch        *b_trackJetJFNVtx;   //!
   TBranch        *b_trackJetJFNTrkAtVtx;   //!
   TBranch        *b_trackJetJFNSingleTrk;   //!
   TBranch        *b_trackJetJFSig3D;   //!
   TBranch        *b_trackJetJFPhi;   //!
   TBranch        *b_trackJetJFPhiErr;   //!
   TBranch        *b_trackJetJFTheta;   //!
   TBranch        *b_trackJetJFThetaErr;   //!
   TBranch        *b_trackJetSV1Efrc;   //!
   TBranch        *b_trackJetSV1Mass;   //!
   TBranch        *b_trackJetSV1NTrkAtVtx;   //!
   TBranch        *b_trackJetSV1Sig3D;   //!
   TBranch        *b_trackJetBHadronPdgId;   //!
   TBranch        *b_trackJetBHadronBarcode;   //!
   TBranch        *b_trackJetBHadronPt;   //!
   TBranch        *b_trackJetBHadronPx;   //!
   TBranch        *b_trackJetBHadronPy;   //!
   TBranch        *b_trackJetBHadronPz;   //!
   TBranch        *b_trackJetBHadronE;   //!
   TBranch        *b_trackJetBHadronM;   //!
   TBranch        *b_trackJetBHadronProdVtxX;   //!
   TBranch        *b_trackJetBHadronProdVtxY;   //!
   TBranch        *b_trackJetBHadronProdVtxZ;   //!
   TBranch        *b_trackJetBHadronDecVtxX;   //!
   TBranch        *b_trackJetBHadronDecVtxY;   //!
   TBranch        *b_trackJetBHadronDecVtxZ;   //!
   TBranch        *b_trackJetBCHadronDecVtxX;   //!
   TBranch        *b_trackJetBCHadronDecVtxY;   //!
   TBranch        *b_trackJetBCHadronDecVtxZ;   //!
   TBranch        *b_trackJetBHadronNSec;   //!
   TBranch        *b_trackJetBHadronNTer;   //!
   TBranch        *b_trackJetBHadronNChSec;   //!
   TBranch        *b_trackJetBHadronNChTer;   //!
   TBranch        *b_trackJetBHadronNLepSec;   //!
   TBranch        *b_trackJetBHadronNLepTer;   //!
   TBranch        *b_trackJetBHadronNK0s;   //!
   TBranch        *b_trackJetBHadronMassDec;   //!
   TBranch        *b_trackJetBHadronMassSec;   //!
   TBranch        *b_trackJetBHadronMassTer;   //!
   TBranch        *b_vrJetPt;   //!
   TBranch        *b_vrJetPx;   //!
   TBranch        *b_vrJetPy;   //!
   TBranch        *b_vrJetPz;   //!
   TBranch        *b_vrJetE;   //!
   TBranch        *b_vrJetM;   //!
   TBranch        *b_vrJetNTracks;   //!
   TBranch        *b_vrJetIdFatJet;   //!
   TBranch        *b_vrJetMV2c10;   //!
   TBranch        *b_vrJetMV2c10mu;   //!
   TBranch        *b_vrJetMV2c10rnn;   //!
   TBranch        *b_vrJetBTagger;   //!
   TBranch        *b_vrJetHybBEff_60;   //!
   TBranch        *b_vrJetHybBEff_70;   //!
   TBranch        *b_vrJetHybBEff_77;   //!
   TBranch        *b_vrJetHybBEff_85;   //!
   TBranch        *b_vrJetSFHybBEff_60;   //!
   TBranch        *b_vrJetSFHybBEff_70;   //!
   TBranch        *b_vrJetSFHybBEff_77;   //!
   TBranch        *b_vrJetSFHybBEff_85;   //!
   TBranch        *b_vrJetIP3Dpu;   //!
   TBranch        *b_vrJetIP3Dpc;   //!
   TBranch        *b_vrJetIP3Dpb;   //!
   TBranch        *b_vrJetRNNIP;   //!
   TBranch        *b_vrJetRNNIPpu;   //!
   TBranch        *b_vrJetRNNIPpc;   //!
   TBranch        *b_vrJetRNNIPpb;   //!
   TBranch        *b_vrJetBHadronPdgId;   //!
   TBranch        *b_vrJetBHadronBarcode;   //!
   TBranch        *b_vrJetBHadronPt;   //!
   TBranch        *b_vrJetBHadronPx;   //!
   TBranch        *b_vrJetBHadronPy;   //!
   TBranch        *b_vrJetBHadronPz;   //!
   TBranch        *b_vrJetBHadronE;   //!
   TBranch        *b_vrJetBHadronM;   //!
   TBranch        *b_vrJetBHadronProdVtxX;   //!
   TBranch        *b_vrJetBHadronProdVtxY;   //!
   TBranch        *b_vrJetBHadronProdVtxZ;   //!
   TBranch        *b_vrJetBHadronDecVtxX;   //!
   TBranch        *b_vrJetBHadronDecVtxY;   //!
   TBranch        *b_vrJetBHadronDecVtxZ;   //!
   TBranch        *b_vrJetBCHadronDecVtxX;   //!
   TBranch        *b_vrJetBCHadronDecVtxY;   //!
   TBranch        *b_vrJetBCHadronDecVtxZ;   //!
   TBranch        *b_vrJetBHadronNSec;   //!
   TBranch        *b_vrJetBHadronNTer;   //!
   TBranch        *b_vrJetBHadronNChSec;   //!
   TBranch        *b_vrJetBHadronNChTer;   //!
   TBranch        *b_vrJetBHadronNLepSec;   //!
   TBranch        *b_vrJetBHadronNLepTer;   //!
   TBranch        *b_vrJetBHadronNK0s;   //!
   TBranch        *b_vrJetBHadronMassDec;   //!
   TBranch        *b_vrJetBHadronMassSec;   //!
   TBranch        *b_vrJetBHadronMassTer;   //!
   TBranch        *b_trackPx;   //!
   TBranch        *b_trackPy;   //!
   TBranch        *b_trackPz;   //!
   TBranch        *b_trackPt;   //!
   TBranch        *b_trackPhi;   //!
   TBranch        *b_trackTheta;   //!
   TBranch        *b_trackQOverP;   //!
   TBranch        *b_trackEta;   //!
   TBranch        *b_trackNPixelLayers;   //!
   TBranch        *b_trackNPixelHits;   //!
   TBranch        *b_trackNPixelHoles;   //!
   TBranch        *b_trackNIBLHits;   //!
   TBranch        *b_trackNBLHits;   //!
   TBranch        *b_trackNIBLSharedHits;   //!
   TBranch        *b_trackNIBLSplitHits;   //!
   TBranch        *b_trackNBLSharedHits;   //!
   TBranch        *b_trackNBLSplitHits;   //!
   TBranch        *b_trackNIBLExpectedHits;   //!
   TBranch        *b_trackNBLExpectedHits;   //!
   TBranch        *b_trackNSCTHits;   //!
   TBranch        *b_trackNSCTHoles;   //!
   TBranch        *b_trackNSCTSharedHits;   //!
   TBranch        *b_trackNTRTHits;   //!
   TBranch        *b_trackD0;   //!
   TBranch        *b_trackZ0;   //!
   TBranch        *b_trackD0Err;   //!
   TBranch        *b_trackZ0Err;   //!
   TBranch        *b_trackTruthPdgId;   //!
   TBranch        *b_trackTruthBarcode;   //!
   TBranch        *b_trackTruthParentFlavour;   //!
   TBranch        *b_trackTruthParentBarcode;   //!

   nominal(TTree *tree=0);
   virtual ~nominal();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef nominal_cxx
nominal::nominal(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.asciandr.17456028._000002.evttree.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.asciandr.17456028._000002.evttree.root");
      }
      f->GetObject("evttree",tree);

   }
   Init(tree);
}

nominal::~nominal()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t nominal::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t nominal::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jetCalConf = 0;
   fatJetCalConf = 0;
   bosonPdgId = 0;
   bosonStatus = 0;
   bosonBarcode = 0;
   bosonPt = 0;
   bosonPx = 0;
   bosonPy = 0;
   bosonPz = 0;
   bosonE = 0;
   bosonM = 0;
   bosonChildPdgId = 0;
   partonPdgId = 0;
   partonStatus = 0;
   partonBarcode = 0;
   partonPt = 0;
   partonPx = 0;
   partonPy = 0;
   partonPz = 0;
   partonE = 0;
   partonM = 0;
   partonParentPdgId = 0;
   partonParentBarcode = 0;
   leptonPdgId = 0;
   leptonStatus = 0;
   leptonBarcode = 0;
   leptonPt = 0;
   leptonPx = 0;
   leptonPy = 0;
   leptonPz = 0;
   leptonE = 0;
   leptonM = 0;
   leptonParentPdgId = 0;
   leptonParentBarcode = 0;
   bHadronPdgId = 0;
   bHadronBarcode = 0;
   bHadronPt = 0;
   bHadronPx = 0;
   bHadronPy = 0;
   bHadronPz = 0;
   bHadronE = 0;
   bHadronM = 0;
   bHadronNSec = 0;
   bHadronNTer = 0;
   bHadronNChSec = 0;
   bHadronNChTer = 0;
   bHadronNLepSec = 0;
   bHadronNLepTer = 0;
   bHadronNK0s = 0;
   jetPt = 0;
   jetPx = 0;
   jetPy = 0;
   jetPz = 0;
   jetE = 0;
   jetM = 0;
   jetQLoosek03 = 0;
   jetQLoosek05 = 0;
   jetQLoosek07 = 0;
   jetQLooseVtxk03 = 0;
   jetQLooseVtxk05 = 0;
   jetQLooseVtxk07 = 0;
   jetNTracks = 0;
   jetNLooseTracks = 0;
   jetNLooseVtxTracks = 0;
   jetJVT = 0;
   jetFlavour = 0;
   jetMV2c10 = 0;
   jetMV2c10mu = 0;
   jetMV2c10rnn = 0;
   jetHybBEffTagger = 0;
   jetHybBEff_60 = 0;
   jetHybBEff_70 = 0;
   jetHybBEff_77 = 0;
   jetHybBEff_85 = 0;
   jetSFHybBEff_60 = 0;
   jetSFHybBEff_70 = 0;
   jetSFHybBEff_77 = 0;
   jetSFHybBEff_85 = 0;
   jetIP3Dpu = 0;
   jetIP3Dpc = 0;
   jetIP3Dpb = 0;
   jetIPRNN = 0;
   jetIPRNNpb = 0;
   jetIPRNNpc = 0;
   jetIPRNNpu = 0;
   jetJFEfrc = 0;
   jetJFMass = 0;
   jetJFdEta = 0;
   jetJFdPhi = 0;
   jetJFNVtx = 0;
   jetJFNTrkAtVtx = 0;
   jetJFNSingleTrk = 0;
   jetJFSig3D = 0;
   jetSV1Efrc = 0;
   jetSV1Mass = 0;
   jetSV1NTrkAtVtx = 0;
   jetSV1Sig3D = 0;
   jetJFPVtxX = 0;
   jetJFPVtxY = 0;
   jetJFPVtxZ = 0;
   jetJFPVtxXErr = 0;
   jetJFPVtxYErr = 0;
   jetJFPVtxZErr = 0;
   jetJFPhi = 0;
   jetJFPhiErr = 0;
   jetJFTheta = 0;
   jetJFThetaErr = 0;
   jetJFSecVtxChi2 = 0;
   jetJFSecVtxNdf = 0;
   jetJFSecVtxNTrk = 0;
   jetJFSecVtxX = 0;
   jetJFSecVtxY = 0;
   jetJFSecVtxZ = 0;
   jetJFSecVtxXErr = 0;
   jetJFSecVtxYErr = 0;
   jetJFSecVtxZErr = 0;
   jetJFSecVtxL3D = 0;
   jetJFSecVtxL3DErr = 0;
   jetJFSecVtxLxy = 0;
   jetJFSecVtxLxyErr = 0;
   jetJFSecVtxMass = 0;
   jetJFSecVtxE = 0;
   jetJFSecVtxEfrac = 0;
   jetJFTerVtxChi2 = 0;
   jetJFTerVtxNdf = 0;
   jetJFTerVtxNTrk = 0;
   jetJFTerVtxX = 0;
   jetJFTerVtxY = 0;
   jetJFTerVtxZ = 0;
   jetJFTerVtxXErr = 0;
   jetJFTerVtxYErr = 0;
   jetJFTerVtxZErr = 0;
   jetJFTerVtxL3D = 0;
   jetJFTerVtxL3DErr = 0;
   jetJFTerVtxLxy = 0;
   jetJFTerVtxLxyErr = 0;
   jetJFTerVtxMass = 0;
   jetJFTerVtxE = 0;
   jetJFTerVtxEfrac = 0;
   muonPt = 0;
   muonPx = 0;
   muonPy = 0;
   muonPz = 0;
   muonCharge = 0;
   muonSMT = 0;
   muonD0 = 0;
   muonPtRel = 0;
   electronPt = 0;
   electronPx = 0;
   electronPy = 0;
   electronPz = 0;
   electronCharge = 0;
   fatJetPt = 0;
   fatJetPx = 0;
   fatJetPy = 0;
   fatJetPz = 0;
   fatJetE = 0;
   fatJetM = 0;
   fatJetXbbM = 0;
   fatJetPtRecoM = 0;
   fatJetNTracks = 0;
   fatJetD2 = 0;
   fatJetNTrkJets = 0;
   fatJetNVRJets = 0;
   fatJetNGhostTop = 0;
   fatJetNGhostW = 0;
   fatJetNGhostZ = 0;
   fatJetNGhostH = 0;
   fatJetDiTrkJetMass = 0;
   fatJetDiTrkJetPt = 0;
   fatJetDiVRJetMass = 0;
   fatJetDiVRJetPt = 0;
   trackJetPt = 0;
   trackJetPx = 0;
   trackJetPy = 0;
   trackJetPz = 0;
   trackJetE = 0;
   trackJetM = 0;
   trackJetNTracks = 0;
   trackJetIdFatJet = 0;
   trackJetFlavour = 0;
   trackJetMV2c10 = 0;
   trackJetMV2c10mu = 0;
   trackJetMV2c10rnn = 0;
   trackJetBTagger = 0;
   trackJetHybBEff_60 = 0;
   trackJetHybBEff_70 = 0;
   trackJetHybBEff_77 = 0;
   trackJetHybBEff_85 = 0;
   trackJetSFHybBEff_60 = 0;
   trackJetSFHybBEff_70 = 0;
   trackJetSFHybBEff_77 = 0;
   trackJetSFHybBEff_85 = 0;
   trackJetIP3Dpu = 0;
   trackJetIP3Dpc = 0;
   trackJetIP3Dpb = 0;
   trackJetRNNIP = 0;
   trackJetRNNIPpu = 0;
   trackJetRNNIPpc = 0;
   trackJetRNNIPpb = 0;
   trackJetJFEfrc = 0;
   trackJetJFMass = 0;
   trackJetJFdEta = 0;
   trackJetJFdPhi = 0;
   trackJetJFNVtx = 0;
   trackJetJFNTrkAtVtx = 0;
   trackJetJFNSingleTrk = 0;
   trackJetJFSig3D = 0;
   trackJetJFPhi = 0;
   trackJetJFPhiErr = 0;
   trackJetJFTheta = 0;
   trackJetJFThetaErr = 0;
   trackJetSV1Efrc = 0;
   trackJetSV1Mass = 0;
   trackJetSV1NTrkAtVtx = 0;
   trackJetSV1Sig3D = 0;
   trackJetBHadronPdgId = 0;
   trackJetBHadronBarcode = 0;
   trackJetBHadronPt = 0;
   trackJetBHadronPx = 0;
   trackJetBHadronPy = 0;
   trackJetBHadronPz = 0;
   trackJetBHadronE = 0;
   trackJetBHadronM = 0;
   trackJetBHadronProdVtxX = 0;
   trackJetBHadronProdVtxY = 0;
   trackJetBHadronProdVtxZ = 0;
   trackJetBHadronDecVtxX = 0;
   trackJetBHadronDecVtxY = 0;
   trackJetBHadronDecVtxZ = 0;
   trackJetBCHadronDecVtxX = 0;
   trackJetBCHadronDecVtxY = 0;
   trackJetBCHadronDecVtxZ = 0;
   trackJetBHadronNSec = 0;
   trackJetBHadronNTer = 0;
   trackJetBHadronNChSec = 0;
   trackJetBHadronNChTer = 0;
   trackJetBHadronNLepSec = 0;
   trackJetBHadronNLepTer = 0;
   trackJetBHadronNK0s = 0;
   trackJetBHadronMassDec = 0;
   trackJetBHadronMassSec = 0;
   trackJetBHadronMassTer = 0;
   vrJetPt = 0;
   vrJetPx = 0;
   vrJetPy = 0;
   vrJetPz = 0;
   vrJetE = 0;
   vrJetM = 0;
   vrJetNTracks = 0;
   vrJetIdFatJet = 0;
   vrJetMV2c10 = 0;
   vrJetMV2c10mu = 0;
   vrJetMV2c10rnn = 0;
   vrJetBTagger = 0;
   vrJetHybBEff_60 = 0;
   vrJetHybBEff_70 = 0;
   vrJetHybBEff_77 = 0;
   vrJetHybBEff_85 = 0;
   vrJetSFHybBEff_60 = 0;
   vrJetSFHybBEff_70 = 0;
   vrJetSFHybBEff_77 = 0;
   vrJetSFHybBEff_85 = 0;
   vrJetIP3Dpu = 0;
   vrJetIP3Dpc = 0;
   vrJetIP3Dpb = 0;
   vrJetRNNIP = 0;
   vrJetRNNIPpu = 0;
   vrJetRNNIPpc = 0;
   vrJetRNNIPpb = 0;
   vrJetBHadronPdgId = 0;
   vrJetBHadronBarcode = 0;
   vrJetBHadronPt = 0;
   vrJetBHadronPx = 0;
   vrJetBHadronPy = 0;
   vrJetBHadronPz = 0;
   vrJetBHadronE = 0;
   vrJetBHadronM = 0;
   vrJetBHadronProdVtxX = 0;
   vrJetBHadronProdVtxY = 0;
   vrJetBHadronProdVtxZ = 0;
   vrJetBHadronDecVtxX = 0;
   vrJetBHadronDecVtxY = 0;
   vrJetBHadronDecVtxZ = 0;
   vrJetBCHadronDecVtxX = 0;
   vrJetBCHadronDecVtxY = 0;
   vrJetBCHadronDecVtxZ = 0;
   vrJetBHadronNSec = 0;
   vrJetBHadronNTer = 0;
   vrJetBHadronNChSec = 0;
   vrJetBHadronNChTer = 0;
   vrJetBHadronNLepSec = 0;
   vrJetBHadronNLepTer = 0;
   vrJetBHadronNK0s = 0;
   vrJetBHadronMassDec = 0;
   vrJetBHadronMassSec = 0;
   vrJetBHadronMassTer = 0;
   trackPx = 0;
   trackPy = 0;
   trackPz = 0;
   trackPt = 0;
   trackPhi = 0;
   trackTheta = 0;
   trackQOverP = 0;
   trackEta = 0;
   trackNPixelLayers = 0;
   trackNPixelHits = 0;
   trackNPixelHoles = 0;
   trackNIBLHits = 0;
   trackNBLHits = 0;
   trackNIBLSharedHits = 0;
   trackNIBLSplitHits = 0;
   trackNBLSharedHits = 0;
   trackNBLSplitHits = 0;
   trackNIBLExpectedHits = 0;
   trackNBLExpectedHits = 0;
   trackNSCTHits = 0;
   trackNSCTHoles = 0;
   trackNSCTSharedHits = 0;
   trackNTRTHits = 0;
   trackD0 = 0;
   trackZ0 = 0;
   trackD0Err = 0;
   trackZ0Err = 0;
   trackTruthPdgId = 0;
   trackTruthBarcode = 0;
   trackTruthParentFlavour = 0;
   trackTruthParentBarcode = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("totweight", &totweight, &b_totweight);
   fChain->SetBranchAddress("tot_events", &tot_events, &b_tot_events);
   fChain->SetBranchAddress("averagePU", &averagePU, &b_averagePU);
   fChain->SetBranchAddress("eventPU", &eventPU, &b_eventPU);
   fChain->SetBranchAddress("mcFlag", &mcFlag, &b_mcFlag);
   fChain->SetBranchAddress("mcProcess", &mcProcess, &b_mcProcess);
   fChain->SetBranchAddress("jetCalConf", &jetCalConf, &b_jetCalConf);
   fChain->SetBranchAddress("fatJetCalConf", &fatJetCalConf, &b_fatJetCalConf);
   fChain->SetBranchAddress("HLT_j420_a10t_lcw_jes_35smcINF_L1SC111", &HLT_j420_a10t_lcw_jes_35smcINF_L1SC111, &b_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111);
   fChain->SetBranchAddress("HLT_j420_a10t_lcw_jes_35smcINF_L1J100", &HLT_j420_a10t_lcw_jes_35smcINF_L1J100, &b_HLT_j420_a10t_lcw_jes_35smcINF_L1J100);
   fChain->SetBranchAddress("HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111", &HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111, &b_HLT_2j330_a10t_lcw_jes_35smcINF_L1SC111);
   fChain->SetBranchAddress("HLT_2j330_a10t_lcw_jes_35smcINF_L1J100", &HLT_2j330_a10t_lcw_jes_35smcINF_L1J100, &b_HLT_2j330_a10t_lcw_jes_35smcINF_L1J100);
   fChain->SetBranchAddress("HLT_j360_a10t_lcw_jes_60smcINF_j360_a10t_lcw_jes_L1SC111", &HLT_j360_a10t_lcw_jes_60smcINF_j360_a10t_lcw_jes_L1SC111, &b_HLT_j360_a10t_lcw_jes_60smcINF_j360_a10t_lcw_jes_L1SC111);
   fChain->SetBranchAddress("HLT_j370_a10t_lcw_jes_35smcINF_j370_a10t_lcw_jes_L1SC111", &HLT_j370_a10t_lcw_jes_35smcINF_j370_a10t_lcw_jes_L1SC111, &b_HLT_j370_a10t_lcw_jes_35smcINF_j370_a10t_lcw_jes_L1SC111);
   fChain->SetBranchAddress("HLT_2j330_a10t_lcw_jes_30smcINF_L1J100", &HLT_2j330_a10t_lcw_jes_30smcINF_L1J100, &b_HLT_2j330_a10t_lcw_jes_30smcINF_L1J100);
   fChain->SetBranchAddress("HLT_j390_a10t_lcw_jes_30smcINF_L1J100", &HLT_j390_a10t_lcw_jes_30smcINF_L1J100, &b_HLT_j390_a10t_lcw_jes_30smcINF_L1J100);
   fChain->SetBranchAddress("HLT_ht1000_L1J100", &HLT_ht1000_L1J100, &b_HLT_ht1000_L1J100);
   fChain->SetBranchAddress("HLT_j420_a10_lcw_L1J100", &HLT_j420_a10_lcw_L1J100, &b_HLT_j420_a10_lcw_L1J100);
   fChain->SetBranchAddress("HLT_j420_a10r_L1J100", &HLT_j420_a10r_L1J100, &b_HLT_j420_a10r_L1J100);
   fChain->SetBranchAddress("HLT_4j100", &HLT_4j100, &b_HLT_4j100);
   fChain->SetBranchAddress("HLT_j380", &HLT_j380, &b_HLT_j380);
   fChain->SetBranchAddress("HLT_j420", &HLT_j420, &b_HLT_j420);
   fChain->SetBranchAddress("HLT_j440", &HLT_j440, &b_HLT_j440);
   fChain->SetBranchAddress("HLT_j460", &HLT_j460, &b_HLT_j460);
   fChain->SetBranchAddress("HLT_j480", &HLT_j480, &b_HLT_j480);
   fChain->SetBranchAddress("HLT_j420_a10t_lcw_jes_L1J100", &HLT_j420_a10t_lcw_jes_L1J100, &b_HLT_j420_a10t_lcw_jes_L1J100);
   fChain->SetBranchAddress("HLT_j440_a10t_lcw_jes_L1J100", &HLT_j440_a10t_lcw_jes_L1J100, &b_HLT_j440_a10t_lcw_jes_L1J100);
   fChain->SetBranchAddress("HLT_j460_a10t_lcw_jes_L1J100", &HLT_j460_a10t_lcw_jes_L1J100, &b_HLT_j460_a10t_lcw_jes_L1J100);
   fChain->SetBranchAddress("HLT_j480_a10_lcw_sub_L1J100", &HLT_j480_a10_lcw_sub_L1J100, &b_HLT_j480_a10_lcw_sub_L1J100);
   fChain->SetBranchAddress("HLT_j420_a10_lcw_subjes_L1J100", &HLT_j420_a10_lcw_subjes_L1J100, &b_HLT_j420_a10_lcw_subjes_L1J100);
   fChain->SetBranchAddress("HLT_j440_a10_lcw_subjes_L1J100", &HLT_j440_a10_lcw_subjes_L1J100, &b_HLT_j440_a10_lcw_subjes_L1J100);
   fChain->SetBranchAddress("HLT_j460_a10_lcw_subjes_L1J100", &HLT_j460_a10_lcw_subjes_L1J100, &b_HLT_j460_a10_lcw_subjes_L1J100);
   fChain->SetBranchAddress("HLT_j480_a10_lcw_subjes_L1J100", &HLT_j480_a10_lcw_subjes_L1J100, &b_HLT_j480_a10_lcw_subjes_L1J100);
   fChain->SetBranchAddress("HLT_j460_a10r_L1SC111", &HLT_j460_a10r_L1SC111, &b_HLT_j460_a10r_L1SC111);
   fChain->SetBranchAddress("HLT_j460_a10r_L1J100", &HLT_j460_a10r_L1J100, &b_HLT_j460_a10r_L1J100);
   fChain->SetBranchAddress("HLT_j460_a10_lcw_subjes_L1SC111", &HLT_j460_a10_lcw_subjes_L1SC111, &b_HLT_j460_a10_lcw_subjes_L1SC111);
   fChain->SetBranchAddress("HLT_j460_a10t_lcw_jes_L1SC111", &HLT_j460_a10t_lcw_jes_L1SC111, &b_HLT_j460_a10t_lcw_jes_L1SC111);
   fChain->SetBranchAddress("HLT_j480_a10t_lcw_jes_L1J100", &HLT_j480_a10t_lcw_jes_L1J100, &b_HLT_j480_a10t_lcw_jes_L1J100);
   fChain->SetBranchAddress("HLT_j440_a10t_lcw_jes_40smcINF_L1J100", &HLT_j440_a10t_lcw_jes_40smcINF_L1J100, &b_HLT_j440_a10t_lcw_jes_40smcINF_L1J100);
   fChain->SetBranchAddress("HLT_2j330_a10t_lcw_jes_40smcINF_L1J100", &HLT_2j330_a10t_lcw_jes_40smcINF_L1J100, &b_HLT_2j330_a10t_lcw_jes_40smcINF_L1J100);
   fChain->SetBranchAddress("HLT_j420_a10t_lcw_jes_40smcINF_L1J100", &HLT_j420_a10t_lcw_jes_40smcINF_L1J100, &b_HLT_j420_a10t_lcw_jes_40smcINF_L1J100);
   fChain->SetBranchAddress("eventMCWeight", &eventMCWeight, &b_eventMCWeight);
   fChain->SetBranchAddress("truthVtxX", &truthVtxX, &b_truthVtxX);
   fChain->SetBranchAddress("truthVtxY", &truthVtxY, &b_truthVtxY);
   fChain->SetBranchAddress("truthVtxZ", &truthVtxZ, &b_truthVtxZ);
   fChain->SetBranchAddress("pVtxZ", &pVtxZ, &b_pVtxZ);
   fChain->SetBranchAddress("MET", &MET, &b_MET);
   fChain->SetBranchAddress("phiMET", &phiMET, &b_phiMET);
   fChain->SetBranchAddress("bosonPdgId", &bosonPdgId, &b_bosonPdgId);
   fChain->SetBranchAddress("bosonStatus", &bosonStatus, &b_bosonStatus);
   fChain->SetBranchAddress("bosonBarcode", &bosonBarcode, &b_bosonBarcode);
   fChain->SetBranchAddress("bosonPt", &bosonPt, &b_bosonPt);
   fChain->SetBranchAddress("bosonPx", &bosonPx, &b_bosonPx);
   fChain->SetBranchAddress("bosonPy", &bosonPy, &b_bosonPy);
   fChain->SetBranchAddress("bosonPz", &bosonPz, &b_bosonPz);
   fChain->SetBranchAddress("bosonE", &bosonE, &b_bosonE);
   fChain->SetBranchAddress("bosonM", &bosonM, &b_bosonM);
   fChain->SetBranchAddress("bosonChildPdgId", &bosonChildPdgId, &b_bosonChildPdgId);
   fChain->SetBranchAddress("partonPdgId", &partonPdgId, &b_partonPdgId);
   fChain->SetBranchAddress("partonStatus", &partonStatus, &b_partonStatus);
   fChain->SetBranchAddress("partonBarcode", &partonBarcode, &b_partonBarcode);
   fChain->SetBranchAddress("partonPt", &partonPt, &b_partonPt);
   fChain->SetBranchAddress("partonPx", &partonPx, &b_partonPx);
   fChain->SetBranchAddress("partonPy", &partonPy, &b_partonPy);
   fChain->SetBranchAddress("partonPz", &partonPz, &b_partonPz);
   fChain->SetBranchAddress("partonE", &partonE, &b_partonE);
   fChain->SetBranchAddress("partonM", &partonM, &b_partonM);
   fChain->SetBranchAddress("partonParentPdgId", &partonParentPdgId, &b_partonParentPdgId);
   fChain->SetBranchAddress("partonParentBarcode", &partonParentBarcode, &b_partonParentBarcode);
   fChain->SetBranchAddress("leptonPdgId", &leptonPdgId, &b_leptonPdgId);
   fChain->SetBranchAddress("leptonStatus", &leptonStatus, &b_leptonStatus);
   fChain->SetBranchAddress("leptonBarcode", &leptonBarcode, &b_leptonBarcode);
   fChain->SetBranchAddress("leptonPt", &leptonPt, &b_leptonPt);
   fChain->SetBranchAddress("leptonPx", &leptonPx, &b_leptonPx);
   fChain->SetBranchAddress("leptonPy", &leptonPy, &b_leptonPy);
   fChain->SetBranchAddress("leptonPz", &leptonPz, &b_leptonPz);
   fChain->SetBranchAddress("leptonE", &leptonE, &b_leptonE);
   fChain->SetBranchAddress("leptonM", &leptonM, &b_leptonM);
   fChain->SetBranchAddress("leptonParentPdgId", &leptonParentPdgId, &b_leptonParentPdgId);
   fChain->SetBranchAddress("leptonParentBarcode", &leptonParentBarcode, &b_leptonParentBarcode);
   fChain->SetBranchAddress("bHadronPdgId", &bHadronPdgId, &b_bHadronPdgId);
   fChain->SetBranchAddress("bHadronBarcode", &bHadronBarcode, &b_bHadronBarcode);
   fChain->SetBranchAddress("bHadronPt", &bHadronPt, &b_bHadronPt);
   fChain->SetBranchAddress("bHadronPx", &bHadronPx, &b_bHadronPx);
   fChain->SetBranchAddress("bHadronPy", &bHadronPy, &b_bHadronPy);
   fChain->SetBranchAddress("bHadronPz", &bHadronPz, &b_bHadronPz);
   fChain->SetBranchAddress("bHadronE", &bHadronE, &b_bHadronE);
   fChain->SetBranchAddress("bHadronM", &bHadronM, &b_bHadronM);
   fChain->SetBranchAddress("bHadronNSec", &bHadronNSec, &b_bHadronNSec);
   fChain->SetBranchAddress("bHadronNTer", &bHadronNTer, &b_bHadronNTer);
   fChain->SetBranchAddress("bHadronNChSec", &bHadronNChSec, &b_bHadronNChSec);
   fChain->SetBranchAddress("bHadronNChTer", &bHadronNChTer, &b_bHadronNChTer);
   fChain->SetBranchAddress("bHadronNLepSec", &bHadronNLepSec, &b_bHadronNLepSec);
   fChain->SetBranchAddress("bHadronNLepTer", &bHadronNLepTer, &b_bHadronNLepTer);
   fChain->SetBranchAddress("bHadronNK0s", &bHadronNK0s, &b_bHadronNK0s);
   fChain->SetBranchAddress("tagCode", &tagCode, &b_tagCode);
   fChain->SetBranchAddress("tagDiJetMH", &tagDiJetMH, &b_tagDiJetMH);
   fChain->SetBranchAddress("tagDJetPtH", &tagDJetPtH, &b_tagDJetPtH);
   fChain->SetBranchAddress("tagDiJetPt1", &tagDiJetPt1, &b_tagDiJetPt1);
   fChain->SetBranchAddress("tagDiJetPt2", &tagDiJetPt2, &b_tagDiJetPt2);
   fChain->SetBranchAddress("jetPt", &jetPt, &b_jetPt);
   fChain->SetBranchAddress("jetPx", &jetPx, &b_jetPx);
   fChain->SetBranchAddress("jetPy", &jetPy, &b_jetPy);
   fChain->SetBranchAddress("jetPz", &jetPz, &b_jetPz);
   fChain->SetBranchAddress("jetE", &jetE, &b_jetE);
   fChain->SetBranchAddress("jetM", &jetM, &b_jetM);
   fChain->SetBranchAddress("jetQLoosek03", &jetQLoosek03, &b_jetQLoosek03);
   fChain->SetBranchAddress("jetQLoosek05", &jetQLoosek05, &b_jetQLoosek05);
   fChain->SetBranchAddress("jetQLoosek07", &jetQLoosek07, &b_jetQLoosek07);
   fChain->SetBranchAddress("jetQLooseVtxk03", &jetQLooseVtxk03, &b_jetQLooseVtxk03);
   fChain->SetBranchAddress("jetQLooseVtxk05", &jetQLooseVtxk05, &b_jetQLooseVtxk05);
   fChain->SetBranchAddress("jetQLooseVtxk07", &jetQLooseVtxk07, &b_jetQLooseVtxk07);
   fChain->SetBranchAddress("jetNTracks", &jetNTracks, &b_jetNTracks);
   fChain->SetBranchAddress("jetNLooseTracks", &jetNLooseTracks, &b_jetNLooseTracks);
   fChain->SetBranchAddress("jetNLooseVtxTracks", &jetNLooseVtxTracks, &b_jetNLooseVtxTracks);
   fChain->SetBranchAddress("jetJVT", &jetJVT, &b_jetJVT);
   fChain->SetBranchAddress("jetFlavour", &jetFlavour, &b_jetFlavour);
   fChain->SetBranchAddress("jetMV2c10", &jetMV2c10, &b_jetMV2c10);
   fChain->SetBranchAddress("jetMV2c10mu", &jetMV2c10mu, &b_jetMV2c10mu);
   fChain->SetBranchAddress("jetMV2c10rnn", &jetMV2c10rnn, &b_jetMV2c10rnn);
   fChain->SetBranchAddress("jetHybBEffTagger", &jetHybBEffTagger, &b_jetHybBEffTagger);
   fChain->SetBranchAddress("jetHybBEff_60", &jetHybBEff_60, &b_jetHybBEff_60);
   fChain->SetBranchAddress("jetHybBEff_70", &jetHybBEff_70, &b_jetHybBEff_70);
   fChain->SetBranchAddress("jetHybBEff_77", &jetHybBEff_77, &b_jetHybBEff_77);
   fChain->SetBranchAddress("jetHybBEff_85", &jetHybBEff_85, &b_jetHybBEff_85);
   fChain->SetBranchAddress("jetSFHybBEff_60", &jetSFHybBEff_60, &b_jetSFHybBEff_60);
   fChain->SetBranchAddress("jetSFHybBEff_70", &jetSFHybBEff_70, &b_jetSFHybBEff_70);
   fChain->SetBranchAddress("jetSFHybBEff_77", &jetSFHybBEff_77, &b_jetSFHybBEff_77);
   fChain->SetBranchAddress("jetSFHybBEff_85", &jetSFHybBEff_85, &b_jetSFHybBEff_85);
   fChain->SetBranchAddress("jetIP3Dpu", &jetIP3Dpu, &b_jetIP3Dpu);
   fChain->SetBranchAddress("jetIP3Dpc", &jetIP3Dpc, &b_jetIP3Dpc);
   fChain->SetBranchAddress("jetIP3Dpb", &jetIP3Dpb, &b_jetIP3Dpb);
   fChain->SetBranchAddress("jetIPRNN", &jetIPRNN, &b_jetIPRNN);
   fChain->SetBranchAddress("jetIPRNNpb", &jetIPRNNpb, &b_jetIPRNNpb);
   fChain->SetBranchAddress("jetIPRNNpc", &jetIPRNNpc, &b_jetIPRNNpc);
   fChain->SetBranchAddress("jetIPRNNpu", &jetIPRNNpu, &b_jetIPRNNpu);
   fChain->SetBranchAddress("jetJFEfrc", &jetJFEfrc, &b_jetJFEfrc);
   fChain->SetBranchAddress("jetJFMass", &jetJFMass, &b_jetJFMass);
   fChain->SetBranchAddress("jetJFdEta", &jetJFdEta, &b_jetJFdEta);
   fChain->SetBranchAddress("jetJFdPhi", &jetJFdPhi, &b_jetJFdPhi);
   fChain->SetBranchAddress("jetJFNVtx", &jetJFNVtx, &b_jetJFNVtx);
   fChain->SetBranchAddress("jetJFNTrkAtVtx", &jetJFNTrkAtVtx, &b_jetJFNTrkAtVtx);
   fChain->SetBranchAddress("jetJFNSingleTrk", &jetJFNSingleTrk, &b_jetJFNSingleTrk);
   fChain->SetBranchAddress("jetJFSig3D", &jetJFSig3D, &b_jetJFSig3D);
   fChain->SetBranchAddress("jetSV1Efrc", &jetSV1Efrc, &b_jetSV1Efrc);
   fChain->SetBranchAddress("jetSV1Mass", &jetSV1Mass, &b_jetSV1Mass);
   fChain->SetBranchAddress("jetSV1NTrkAtVtx", &jetSV1NTrkAtVtx, &b_jetSV1NTrkAtVtx);
   fChain->SetBranchAddress("jetSV1Sig3D", &jetSV1Sig3D, &b_jetSV1Sig3D);
   fChain->SetBranchAddress("jetJFPVtxX", &jetJFPVtxX, &b_jetJFPVtxX);
   fChain->SetBranchAddress("jetJFPVtxY", &jetJFPVtxY, &b_jetJFPVtxY);
   fChain->SetBranchAddress("jetJFPVtxZ", &jetJFPVtxZ, &b_jetJFPVtxZ);
   fChain->SetBranchAddress("jetJFPVtxXErr", &jetJFPVtxXErr, &b_jetJFPVtxXErr);
   fChain->SetBranchAddress("jetJFPVtxYErr", &jetJFPVtxYErr, &b_jetJFPVtxYErr);
   fChain->SetBranchAddress("jetJFPVtxZErr", &jetJFPVtxZErr, &b_jetJFPVtxZErr);
   fChain->SetBranchAddress("jetJFPhi", &jetJFPhi, &b_jetJFPhi);
   fChain->SetBranchAddress("jetJFPhiErr", &jetJFPhiErr, &b_jetJFPhiErr);
   fChain->SetBranchAddress("jetJFTheta", &jetJFTheta, &b_jetJFTheta);
   fChain->SetBranchAddress("jetJFThetaErr", &jetJFThetaErr, &b_jetJFThetaErr);
   fChain->SetBranchAddress("jetJFSecVtxChi2", &jetJFSecVtxChi2, &b_jetJFSecVtxChi2);
   fChain->SetBranchAddress("jetJFSecVtxNdf", &jetJFSecVtxNdf, &b_jetJFSecVtxNdf);
   fChain->SetBranchAddress("jetJFSecVtxNTrk", &jetJFSecVtxNTrk, &b_jetJFSecVtxNTrk);
   fChain->SetBranchAddress("jetJFSecVtxX", &jetJFSecVtxX, &b_jetJFSecVtxX);
   fChain->SetBranchAddress("jetJFSecVtxY", &jetJFSecVtxY, &b_jetJFSecVtxY);
   fChain->SetBranchAddress("jetJFSecVtxZ", &jetJFSecVtxZ, &b_jetJFSecVtxZ);
   fChain->SetBranchAddress("jetJFSecVtxXErr", &jetJFSecVtxXErr, &b_jetJFSecVtxXErr);
   fChain->SetBranchAddress("jetJFSecVtxYErr", &jetJFSecVtxYErr, &b_jetJFSecVtxYErr);
   fChain->SetBranchAddress("jetJFSecVtxZErr", &jetJFSecVtxZErr, &b_jetJFSecVtxZErr);
   fChain->SetBranchAddress("jetJFSecVtxL3D", &jetJFSecVtxL3D, &b_jetJFSecVtxL3D);
   fChain->SetBranchAddress("jetJFSecVtxL3DErr", &jetJFSecVtxL3DErr, &b_jetJFSecVtxL3DErr);
   fChain->SetBranchAddress("jetJFSecVtxLxy", &jetJFSecVtxLxy, &b_jetJFSecVtxLxy);
   fChain->SetBranchAddress("jetJFSecVtxLxyErr", &jetJFSecVtxLxyErr, &b_jetJFSecVtxLxyErr);
   fChain->SetBranchAddress("jetJFSecVtxMass", &jetJFSecVtxMass, &b_jetJFSecVtxMass);
   fChain->SetBranchAddress("jetJFSecVtxE", &jetJFSecVtxE, &b_jetJFSecVtxE);
   fChain->SetBranchAddress("jetJFSecVtxEfrac", &jetJFSecVtxEfrac, &b_jetJFSecVtxEfrac);
   fChain->SetBranchAddress("jetJFTerVtxChi2", &jetJFTerVtxChi2, &b_jetJFTerVtxChi2);
   fChain->SetBranchAddress("jetJFTerVtxNdf", &jetJFTerVtxNdf, &b_jetJFTerVtxNdf);
   fChain->SetBranchAddress("jetJFTerVtxNTrk", &jetJFTerVtxNTrk, &b_jetJFTerVtxNTrk);
   fChain->SetBranchAddress("jetJFTerVtxX", &jetJFTerVtxX, &b_jetJFTerVtxX);
   fChain->SetBranchAddress("jetJFTerVtxY", &jetJFTerVtxY, &b_jetJFTerVtxY);
   fChain->SetBranchAddress("jetJFTerVtxZ", &jetJFTerVtxZ, &b_jetJFTerVtxZ);
   fChain->SetBranchAddress("jetJFTerVtxXErr", &jetJFTerVtxXErr, &b_jetJFTerVtxXErr);
   fChain->SetBranchAddress("jetJFTerVtxYErr", &jetJFTerVtxYErr, &b_jetJFTerVtxYErr);
   fChain->SetBranchAddress("jetJFTerVtxZErr", &jetJFTerVtxZErr, &b_jetJFTerVtxZErr);
   fChain->SetBranchAddress("jetJFTerVtxL3D", &jetJFTerVtxL3D, &b_jetJFTerVtxL3D);
   fChain->SetBranchAddress("jetJFTerVtxL3DErr", &jetJFTerVtxL3DErr, &b_jetJFTerVtxL3DErr);
   fChain->SetBranchAddress("jetJFTerVtxLxy", &jetJFTerVtxLxy, &b_jetJFTerVtxLxy);
   fChain->SetBranchAddress("jetJFTerVtxLxyErr", &jetJFTerVtxLxyErr, &b_jetJFTerVtxLxyErr);
   fChain->SetBranchAddress("jetJFTerVtxMass", &jetJFTerVtxMass, &b_jetJFTerVtxMass);
   fChain->SetBranchAddress("jetJFTerVtxE", &jetJFTerVtxE, &b_jetJFTerVtxE);
   fChain->SetBranchAddress("jetJFTerVtxEfrac", &jetJFTerVtxEfrac, &b_jetJFTerVtxEfrac);
   fChain->SetBranchAddress("muonPt", &muonPt, &b_muonPt);
   fChain->SetBranchAddress("muonPx", &muonPx, &b_muonPx);
   fChain->SetBranchAddress("muonPy", &muonPy, &b_muonPy);
   fChain->SetBranchAddress("muonPz", &muonPz, &b_muonPz);
   fChain->SetBranchAddress("muonCharge", &muonCharge, &b_muonCharge);
   fChain->SetBranchAddress("muonSMT", &muonSMT, &b_muonSMT);
   fChain->SetBranchAddress("muonD0", &muonD0, &b_muonD0);
   fChain->SetBranchAddress("muonPtRel", &muonPtRel, &b_muonPtRel);
   fChain->SetBranchAddress("electronPt", &electronPt, &b_electronPt);
   fChain->SetBranchAddress("electronPx", &electronPx, &b_electronPx);
   fChain->SetBranchAddress("electronPy", &electronPy, &b_electronPy);
   fChain->SetBranchAddress("electronPz", &electronPz, &b_electronPz);
   fChain->SetBranchAddress("electronCharge", &electronCharge, &b_electronCharge);
   fChain->SetBranchAddress("diMuonMass", &diMuonMass, &b_diMuonMass);
   fChain->SetBranchAddress("diElectronMass", &diElectronMass, &b_diElectronMass);
   fChain->SetBranchAddress("muonElectronMass", &muonElectronMass, &b_muonElectronMass);
   fChain->SetBranchAddress("fatJetPt", &fatJetPt, &b_fatJetPt);
   fChain->SetBranchAddress("fatJetPx", &fatJetPx, &b_fatJetPx);
   fChain->SetBranchAddress("fatJetPy", &fatJetPy, &b_fatJetPy);
   fChain->SetBranchAddress("fatJetPz", &fatJetPz, &b_fatJetPz);
   fChain->SetBranchAddress("fatJetE", &fatJetE, &b_fatJetE);
   fChain->SetBranchAddress("fatJetM", &fatJetM, &b_fatJetM);
   fChain->SetBranchAddress("fatJetXbbM", &fatJetXbbM, &b_fatJetXbbM);
   fChain->SetBranchAddress("fatJetPtRecoM", &fatJetPtRecoM, &b_fatJetPtRecoM);
   fChain->SetBranchAddress("fatJetNTracks", &fatJetNTracks, &b_fatJetNTracks);
   fChain->SetBranchAddress("fatJetD2", &fatJetD2, &b_fatJetD2);
   fChain->SetBranchAddress("fatJetNTrkJets", &fatJetNTrkJets, &b_fatJetNTrkJets);
   fChain->SetBranchAddress("fatJetNVRJets", &fatJetNVRJets, &b_fatJetNVRJets);
   fChain->SetBranchAddress("fatJetNGhostTop", &fatJetNGhostTop, &b_fatJetNGhostTop);
   fChain->SetBranchAddress("fatJetNGhostW", &fatJetNGhostW, &b_fatJetNGhostW);
   fChain->SetBranchAddress("fatJetNGhostZ", &fatJetNGhostZ, &b_fatJetNGhostZ);
   fChain->SetBranchAddress("fatJetNGhostH", &fatJetNGhostH, &b_fatJetNGhostH);
   fChain->SetBranchAddress("fatJetDiTrkJetMass", &fatJetDiTrkJetMass, &b_fatJetDiTrkJetMass);
   fChain->SetBranchAddress("fatJetDiTrkJetPt", &fatJetDiTrkJetPt, &b_fatJetDiTrkJetPt);
   fChain->SetBranchAddress("fatJetDiVRJetMass", &fatJetDiVRJetMass, &b_fatJetDiVRJetMass);
   fChain->SetBranchAddress("fatJetDiVRJetPt", &fatJetDiVRJetPt, &b_fatJetDiVRJetPt);
   fChain->SetBranchAddress("trackJetPt", &trackJetPt, &b_trackJetPt);
   fChain->SetBranchAddress("trackJetPx", &trackJetPx, &b_trackJetPx);
   fChain->SetBranchAddress("trackJetPy", &trackJetPy, &b_trackJetPy);
   fChain->SetBranchAddress("trackJetPz", &trackJetPz, &b_trackJetPz);
   fChain->SetBranchAddress("trackJetE", &trackJetE, &b_trackJetE);
   fChain->SetBranchAddress("trackJetM", &trackJetM, &b_trackJetM);
   fChain->SetBranchAddress("trackJetNTracks", &trackJetNTracks, &b_trackJetNTracks);
   fChain->SetBranchAddress("trackJetIdFatJet", &trackJetIdFatJet, &b_trackJetIdFatJet);
   fChain->SetBranchAddress("trackJetFlavour", &trackJetFlavour, &b_trackJetFlavour);
   fChain->SetBranchAddress("trackJetMV2c10", &trackJetMV2c10, &b_trackJetMV2c10);
   fChain->SetBranchAddress("trackJetMV2c10mu", &trackJetMV2c10mu, &b_trackJetMV2c10mu);
   fChain->SetBranchAddress("trackJetMV2c10rnn", &trackJetMV2c10rnn, &b_trackJetMV2c10rnn);
   fChain->SetBranchAddress("trackJetBTagger", &trackJetBTagger, &b_trackJetBTagger);
   fChain->SetBranchAddress("trackJetHybBEff_60", &trackJetHybBEff_60, &b_trackJetHybBEff_60);
   fChain->SetBranchAddress("trackJetHybBEff_70", &trackJetHybBEff_70, &b_trackJetHybBEff_70);
   fChain->SetBranchAddress("trackJetHybBEff_77", &trackJetHybBEff_77, &b_trackJetHybBEff_77);
   fChain->SetBranchAddress("trackJetHybBEff_85", &trackJetHybBEff_85, &b_trackJetHybBEff_85);
   fChain->SetBranchAddress("trackJetSFHybBEff_60", &trackJetSFHybBEff_60, &b_trackJetSFHybBEff_60);
   fChain->SetBranchAddress("trackJetSFHybBEff_70", &trackJetSFHybBEff_70, &b_trackJetSFHybBEff_70);
   fChain->SetBranchAddress("trackJetSFHybBEff_77", &trackJetSFHybBEff_77, &b_trackJetSFHybBEff_77);
   fChain->SetBranchAddress("trackJetSFHybBEff_85", &trackJetSFHybBEff_85, &b_trackJetSFHybBEff_85);
   fChain->SetBranchAddress("trackJetIP3Dpu", &trackJetIP3Dpu, &b_trackJetIP3Dpu);
   fChain->SetBranchAddress("trackJetIP3Dpc", &trackJetIP3Dpc, &b_trackJetIP3Dpc);
   fChain->SetBranchAddress("trackJetIP3Dpb", &trackJetIP3Dpb, &b_trackJetIP3Dpb);
   fChain->SetBranchAddress("trackJetRNNIP", &trackJetRNNIP, &b_trackJetRNNIP);
   fChain->SetBranchAddress("trackJetRNNIPpu", &trackJetRNNIPpu, &b_trackJetRNNIPpu);
   fChain->SetBranchAddress("trackJetRNNIPpc", &trackJetRNNIPpc, &b_trackJetRNNIPpc);
   fChain->SetBranchAddress("trackJetRNNIPpb", &trackJetRNNIPpb, &b_trackJetRNNIPpb);
   fChain->SetBranchAddress("trackJetJFEfrc", &trackJetJFEfrc, &b_trackJetJFEfrc);
   fChain->SetBranchAddress("trackJetJFMass", &trackJetJFMass, &b_trackJetJFMass);
   fChain->SetBranchAddress("trackJetJFdEta", &trackJetJFdEta, &b_trackJetJFdEta);
   fChain->SetBranchAddress("trackJetJFdPhi", &trackJetJFdPhi, &b_trackJetJFdPhi);
   fChain->SetBranchAddress("trackJetJFNVtx", &trackJetJFNVtx, &b_trackJetJFNVtx);
   fChain->SetBranchAddress("trackJetJFNTrkAtVtx", &trackJetJFNTrkAtVtx, &b_trackJetJFNTrkAtVtx);
   fChain->SetBranchAddress("trackJetJFNSingleTrk", &trackJetJFNSingleTrk, &b_trackJetJFNSingleTrk);
   fChain->SetBranchAddress("trackJetJFSig3D", &trackJetJFSig3D, &b_trackJetJFSig3D);
   fChain->SetBranchAddress("trackJetJFPhi", &trackJetJFPhi, &b_trackJetJFPhi);
   fChain->SetBranchAddress("trackJetJFPhiErr", &trackJetJFPhiErr, &b_trackJetJFPhiErr);
   fChain->SetBranchAddress("trackJetJFTheta", &trackJetJFTheta, &b_trackJetJFTheta);
   fChain->SetBranchAddress("trackJetJFThetaErr", &trackJetJFThetaErr, &b_trackJetJFThetaErr);
   fChain->SetBranchAddress("trackJetSV1Efrc", &trackJetSV1Efrc, &b_trackJetSV1Efrc);
   fChain->SetBranchAddress("trackJetSV1Mass", &trackJetSV1Mass, &b_trackJetSV1Mass);
   fChain->SetBranchAddress("trackJetSV1NTrkAtVtx", &trackJetSV1NTrkAtVtx, &b_trackJetSV1NTrkAtVtx);
   fChain->SetBranchAddress("trackJetSV1Sig3D", &trackJetSV1Sig3D, &b_trackJetSV1Sig3D);
   fChain->SetBranchAddress("trackJetBHadronPdgId", &trackJetBHadronPdgId, &b_trackJetBHadronPdgId);
   fChain->SetBranchAddress("trackJetBHadronBarcode", &trackJetBHadronBarcode, &b_trackJetBHadronBarcode);
   fChain->SetBranchAddress("trackJetBHadronPt", &trackJetBHadronPt, &b_trackJetBHadronPt);
   fChain->SetBranchAddress("trackJetBHadronPx", &trackJetBHadronPx, &b_trackJetBHadronPx);
   fChain->SetBranchAddress("trackJetBHadronPy", &trackJetBHadronPy, &b_trackJetBHadronPy);
   fChain->SetBranchAddress("trackJetBHadronPz", &trackJetBHadronPz, &b_trackJetBHadronPz);
   fChain->SetBranchAddress("trackJetBHadronE", &trackJetBHadronE, &b_trackJetBHadronE);
   fChain->SetBranchAddress("trackJetBHadronM", &trackJetBHadronM, &b_trackJetBHadronM);
   fChain->SetBranchAddress("trackJetBHadronProdVtxX", &trackJetBHadronProdVtxX, &b_trackJetBHadronProdVtxX);
   fChain->SetBranchAddress("trackJetBHadronProdVtxY", &trackJetBHadronProdVtxY, &b_trackJetBHadronProdVtxY);
   fChain->SetBranchAddress("trackJetBHadronProdVtxZ", &trackJetBHadronProdVtxZ, &b_trackJetBHadronProdVtxZ);
   fChain->SetBranchAddress("trackJetBHadronDecVtxX", &trackJetBHadronDecVtxX, &b_trackJetBHadronDecVtxX);
   fChain->SetBranchAddress("trackJetBHadronDecVtxY", &trackJetBHadronDecVtxY, &b_trackJetBHadronDecVtxY);
   fChain->SetBranchAddress("trackJetBHadronDecVtxZ", &trackJetBHadronDecVtxZ, &b_trackJetBHadronDecVtxZ);
   fChain->SetBranchAddress("trackJetBCHadronDecVtxX", &trackJetBCHadronDecVtxX, &b_trackJetBCHadronDecVtxX);
   fChain->SetBranchAddress("trackJetBCHadronDecVtxY", &trackJetBCHadronDecVtxY, &b_trackJetBCHadronDecVtxY);
   fChain->SetBranchAddress("trackJetBCHadronDecVtxZ", &trackJetBCHadronDecVtxZ, &b_trackJetBCHadronDecVtxZ);
   fChain->SetBranchAddress("trackJetBHadronNSec", &trackJetBHadronNSec, &b_trackJetBHadronNSec);
   fChain->SetBranchAddress("trackJetBHadronNTer", &trackJetBHadronNTer, &b_trackJetBHadronNTer);
   fChain->SetBranchAddress("trackJetBHadronNChSec", &trackJetBHadronNChSec, &b_trackJetBHadronNChSec);
   fChain->SetBranchAddress("trackJetBHadronNChTer", &trackJetBHadronNChTer, &b_trackJetBHadronNChTer);
   fChain->SetBranchAddress("trackJetBHadronNLepSec", &trackJetBHadronNLepSec, &b_trackJetBHadronNLepSec);
   fChain->SetBranchAddress("trackJetBHadronNLepTer", &trackJetBHadronNLepTer, &b_trackJetBHadronNLepTer);
   fChain->SetBranchAddress("trackJetBHadronNK0s", &trackJetBHadronNK0s, &b_trackJetBHadronNK0s);
   fChain->SetBranchAddress("trackJetBHadronMassDec", &trackJetBHadronMassDec, &b_trackJetBHadronMassDec);
   fChain->SetBranchAddress("trackJetBHadronMassSec", &trackJetBHadronMassSec, &b_trackJetBHadronMassSec);
   fChain->SetBranchAddress("trackJetBHadronMassTer", &trackJetBHadronMassTer, &b_trackJetBHadronMassTer);
   fChain->SetBranchAddress("vrJetPt", &vrJetPt, &b_vrJetPt);
   fChain->SetBranchAddress("vrJetPx", &vrJetPx, &b_vrJetPx);
   fChain->SetBranchAddress("vrJetPy", &vrJetPy, &b_vrJetPy);
   fChain->SetBranchAddress("vrJetPz", &vrJetPz, &b_vrJetPz);
   fChain->SetBranchAddress("vrJetE", &vrJetE, &b_vrJetE);
   fChain->SetBranchAddress("vrJetM", &vrJetM, &b_vrJetM);
   fChain->SetBranchAddress("vrJetNTracks", &vrJetNTracks, &b_vrJetNTracks);
   fChain->SetBranchAddress("vrJetIdFatJet", &vrJetIdFatJet, &b_vrJetIdFatJet);
   fChain->SetBranchAddress("vrJetMV2c10", &vrJetMV2c10, &b_vrJetMV2c10);
   fChain->SetBranchAddress("vrJetMV2c10mu", &vrJetMV2c10mu, &b_vrJetMV2c10mu);
   fChain->SetBranchAddress("vrJetMV2c10rnn", &vrJetMV2c10rnn, &b_vrJetMV2c10rnn);
   fChain->SetBranchAddress("vrJetBTagger", &vrJetBTagger, &b_vrJetBTagger);
   fChain->SetBranchAddress("vrJetHybBEff_60", &vrJetHybBEff_60, &b_vrJetHybBEff_60);
   fChain->SetBranchAddress("vrJetHybBEff_70", &vrJetHybBEff_70, &b_vrJetHybBEff_70);
   fChain->SetBranchAddress("vrJetHybBEff_77", &vrJetHybBEff_77, &b_vrJetHybBEff_77);
   fChain->SetBranchAddress("vrJetHybBEff_85", &vrJetHybBEff_85, &b_vrJetHybBEff_85);
   fChain->SetBranchAddress("vrJetSFHybBEff_60", &vrJetSFHybBEff_60, &b_vrJetSFHybBEff_60);
   fChain->SetBranchAddress("vrJetSFHybBEff_70", &vrJetSFHybBEff_70, &b_vrJetSFHybBEff_70);
   fChain->SetBranchAddress("vrJetSFHybBEff_77", &vrJetSFHybBEff_77, &b_vrJetSFHybBEff_77);
   fChain->SetBranchAddress("vrJetSFHybBEff_85", &vrJetSFHybBEff_85, &b_vrJetSFHybBEff_85);
   fChain->SetBranchAddress("vrJetIP3Dpu", &vrJetIP3Dpu, &b_vrJetIP3Dpu);
   fChain->SetBranchAddress("vrJetIP3Dpc", &vrJetIP3Dpc, &b_vrJetIP3Dpc);
   fChain->SetBranchAddress("vrJetIP3Dpb", &vrJetIP3Dpb, &b_vrJetIP3Dpb);
   fChain->SetBranchAddress("vrJetRNNIP", &vrJetRNNIP, &b_vrJetRNNIP);
   fChain->SetBranchAddress("vrJetRNNIPpu", &vrJetRNNIPpu, &b_vrJetRNNIPpu);
   fChain->SetBranchAddress("vrJetRNNIPpc", &vrJetRNNIPpc, &b_vrJetRNNIPpc);
   fChain->SetBranchAddress("vrJetRNNIPpb", &vrJetRNNIPpb, &b_vrJetRNNIPpb);
   fChain->SetBranchAddress("vrJetBHadronPdgId", &vrJetBHadronPdgId, &b_vrJetBHadronPdgId);
   fChain->SetBranchAddress("vrJetBHadronBarcode", &vrJetBHadronBarcode, &b_vrJetBHadronBarcode);
   fChain->SetBranchAddress("vrJetBHadronPt", &vrJetBHadronPt, &b_vrJetBHadronPt);
   fChain->SetBranchAddress("vrJetBHadronPx", &vrJetBHadronPx, &b_vrJetBHadronPx);
   fChain->SetBranchAddress("vrJetBHadronPy", &vrJetBHadronPy, &b_vrJetBHadronPy);
   fChain->SetBranchAddress("vrJetBHadronPz", &vrJetBHadronPz, &b_vrJetBHadronPz);
   fChain->SetBranchAddress("vrJetBHadronE", &vrJetBHadronE, &b_vrJetBHadronE);
   fChain->SetBranchAddress("vrJetBHadronM", &vrJetBHadronM, &b_vrJetBHadronM);
   fChain->SetBranchAddress("vrJetBHadronProdVtxX", &vrJetBHadronProdVtxX, &b_vrJetBHadronProdVtxX);
   fChain->SetBranchAddress("vrJetBHadronProdVtxY", &vrJetBHadronProdVtxY, &b_vrJetBHadronProdVtxY);
   fChain->SetBranchAddress("vrJetBHadronProdVtxZ", &vrJetBHadronProdVtxZ, &b_vrJetBHadronProdVtxZ);
   fChain->SetBranchAddress("vrJetBHadronDecVtxX", &vrJetBHadronDecVtxX, &b_vrJetBHadronDecVtxX);
   fChain->SetBranchAddress("vrJetBHadronDecVtxY", &vrJetBHadronDecVtxY, &b_vrJetBHadronDecVtxY);
   fChain->SetBranchAddress("vrJetBHadronDecVtxZ", &vrJetBHadronDecVtxZ, &b_vrJetBHadronDecVtxZ);
   fChain->SetBranchAddress("vrJetBCHadronDecVtxX", &vrJetBCHadronDecVtxX, &b_vrJetBCHadronDecVtxX);
   fChain->SetBranchAddress("vrJetBCHadronDecVtxY", &vrJetBCHadronDecVtxY, &b_vrJetBCHadronDecVtxY);
   fChain->SetBranchAddress("vrJetBCHadronDecVtxZ", &vrJetBCHadronDecVtxZ, &b_vrJetBCHadronDecVtxZ);
   fChain->SetBranchAddress("vrJetBHadronNSec", &vrJetBHadronNSec, &b_vrJetBHadronNSec);
   fChain->SetBranchAddress("vrJetBHadronNTer", &vrJetBHadronNTer, &b_vrJetBHadronNTer);
   fChain->SetBranchAddress("vrJetBHadronNChSec", &vrJetBHadronNChSec, &b_vrJetBHadronNChSec);
   fChain->SetBranchAddress("vrJetBHadronNChTer", &vrJetBHadronNChTer, &b_vrJetBHadronNChTer);
   fChain->SetBranchAddress("vrJetBHadronNLepSec", &vrJetBHadronNLepSec, &b_vrJetBHadronNLepSec);
   fChain->SetBranchAddress("vrJetBHadronNLepTer", &vrJetBHadronNLepTer, &b_vrJetBHadronNLepTer);
   fChain->SetBranchAddress("vrJetBHadronNK0s", &vrJetBHadronNK0s, &b_vrJetBHadronNK0s);
   fChain->SetBranchAddress("vrJetBHadronMassDec", &vrJetBHadronMassDec, &b_vrJetBHadronMassDec);
   fChain->SetBranchAddress("vrJetBHadronMassSec", &vrJetBHadronMassSec, &b_vrJetBHadronMassSec);
   fChain->SetBranchAddress("vrJetBHadronMassTer", &vrJetBHadronMassTer, &b_vrJetBHadronMassTer);
   fChain->SetBranchAddress("trackPx", &trackPx, &b_trackPx);
   fChain->SetBranchAddress("trackPy", &trackPy, &b_trackPy);
   fChain->SetBranchAddress("trackPz", &trackPz, &b_trackPz);
   fChain->SetBranchAddress("trackPt", &trackPt, &b_trackPt);
   fChain->SetBranchAddress("trackPhi", &trackPhi, &b_trackPhi);
   fChain->SetBranchAddress("trackTheta", &trackTheta, &b_trackTheta);
   fChain->SetBranchAddress("trackQOverP", &trackQOverP, &b_trackQOverP);
   fChain->SetBranchAddress("trackEta", &trackEta, &b_trackEta);
   fChain->SetBranchAddress("trackNPixelLayers", &trackNPixelLayers, &b_trackNPixelLayers);
   fChain->SetBranchAddress("trackNPixelHits", &trackNPixelHits, &b_trackNPixelHits);
   fChain->SetBranchAddress("trackNPixelHoles", &trackNPixelHoles, &b_trackNPixelHoles);
   fChain->SetBranchAddress("trackNIBLHits", &trackNIBLHits, &b_trackNIBLHits);
   fChain->SetBranchAddress("trackNBLHits", &trackNBLHits, &b_trackNBLHits);
   fChain->SetBranchAddress("trackNIBLSharedHits", &trackNIBLSharedHits, &b_trackNIBLSharedHits);
   fChain->SetBranchAddress("trackNIBLSplitHits", &trackNIBLSplitHits, &b_trackNIBLSplitHits);
   fChain->SetBranchAddress("trackNBLSharedHits", &trackNBLSharedHits, &b_trackNBLSharedHits);
   fChain->SetBranchAddress("trackNBLSplitHits", &trackNBLSplitHits, &b_trackNBLSplitHits);
   fChain->SetBranchAddress("trackNIBLExpectedHits", &trackNIBLExpectedHits, &b_trackNIBLExpectedHits);
   fChain->SetBranchAddress("trackNBLExpectedHits", &trackNBLExpectedHits, &b_trackNBLExpectedHits);
   fChain->SetBranchAddress("trackNSCTHits", &trackNSCTHits, &b_trackNSCTHits);
   fChain->SetBranchAddress("trackNSCTHoles", &trackNSCTHoles, &b_trackNSCTHoles);
   fChain->SetBranchAddress("trackNSCTSharedHits", &trackNSCTSharedHits, &b_trackNSCTSharedHits);
   fChain->SetBranchAddress("trackNTRTHits", &trackNTRTHits, &b_trackNTRTHits);
   fChain->SetBranchAddress("trackD0", &trackD0, &b_trackD0);
   fChain->SetBranchAddress("trackZ0", &trackZ0, &b_trackZ0);
   fChain->SetBranchAddress("trackD0Err", &trackD0Err, &b_trackD0Err);
   fChain->SetBranchAddress("trackZ0Err", &trackZ0Err, &b_trackZ0Err);
   fChain->SetBranchAddress("trackTruthPdgId", &trackTruthPdgId, &b_trackTruthPdgId);
   fChain->SetBranchAddress("trackTruthBarcode", &trackTruthBarcode, &b_trackTruthBarcode);
   fChain->SetBranchAddress("trackTruthParentFlavour", &trackTruthParentFlavour, &b_trackTruthParentFlavour);
   fChain->SetBranchAddress("trackTruthParentBarcode", &trackTruthParentBarcode, &b_trackTruthParentBarcode);
   Notify();
}

Bool_t nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void nominal::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t nominal::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef nominal_cxx
