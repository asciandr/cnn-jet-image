### Check feature maps
### See https://www.analyticsvidhya.com/blog/2020/11/tutorial-how-to-visualize-feature-maps-directly-from-cnn-layers/
from tensorflow import keras
from sklearn.model_selection import train_test_split
from os import path
import sys
import numpy as np
import h5py
import math
import matplotlib.pyplot as plt

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

### load keras model
#model = keras.models.load_model('Basic_model_learnRate1.2e-3.h5')
model = keras.models.load_model('Basic_model.h5')

### retrieve input images
hdf5Name = 'dataset.h5'
h5f = h5py.File(hdf5Name,'r')
images = h5f['images'][:]
y = h5f['labels'][:]
images = np.squeeze(images, axis=1)
images = np.expand_dims(images, axis=3)
### split into train, valid, test
### fixed random_state to reproduce results
x_train, x_valid, y_train, y_valid = train_test_split(images, y, test_size=0.30, random_state= 1)

### get feature map from hidden layers
model.summary()
### choose image
#print(y_valid.shape)
n_Hbb=np.where(y_valid==1)
print(n_Hbb)
###n_imgs=[0,33,55]
n_imgs=[19,23,29,43]
for n_img in n_imgs:
  img=x_valid[n_img]
  if y_valid[n_img]==1:
    print('H->bb image')
  else:
    print('g->bb image')
  plt.imshow(np.squeeze(img,axis=2), cmap='jet', vmin=0, vmax=0.01)
  plt.show()
  print(img.shape)
  img = np.expand_dims(img, axis=0)
  print(img.shape)
  layer_names = [layer.name for layer in model.layers]
  layer_outputs = [layer.output for layer in model.layers]
  feature_map_model = keras.models.Model(inputs=model.input, outputs=layer_outputs)
  feature_maps = feature_map_model.predict(img)
  for layer_name, feature_map in zip(layer_names, feature_maps):
    print(f"The shape of the {layer_name} is =======>> {feature_map.shape}")
    if len(feature_map.shape) != 4: continue
    ### plot all square^2 maps in an (square x square) squares
    square = int(math.sqrt(len(feature_map[0][0][0])))
    ix = 1
    for _ in range(square):
	    for _ in range(square):
		    ### specify subplot and turn of axis
		    ax = plt.subplot(square, square, ix)
		    ax.set_xticks([])
		    ax.set_yticks([])
		    ### plot filter channel in grayscale
		    plt.imshow(feature_map[0,:,:,ix-1], cmap='gray')
		    ix += 1
    ### show the figure
    plt.show()

### get model prediction for test/validation sample
test_preds = model.predict(x_valid, batch_size=1000)
### get ROC curve if we have sklearn
if roc_curve:
    print(y_valid.shape)
    print(test_preds[:,1].shape)
    cnn_fp, cnn_tp, threshs = roc_curve(y_valid, test_preds[:,1])

    # get area under the ROC curve
    auc = roc_auc_score(y_valid, test_preds[:,1])
    print()
    print('CNN AUC:', auc)
    print()


    # make ROC curve plot if we have matplotlib
    if plt:

        # some nicer plot settings 
        plt.rcParams['figure.figsize'] = (4,4)
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['figure.autolayout'] = True

        # plot the ROC curves
        plt.plot(cnn_tp, 1-cnn_fp, '-', color='black', label='CNN')

        # axes labels
        plt.xlabel('H->bb Efficiency')
        plt.ylabel('g->bb Rejection')

        # axes limits
        plt.xlim(0, 1)
        plt.ylim(0, 1)

        # make legend and show plot
        plt.legend(loc='lower left', frameon=False)
        plt.show()

### Plot CNN output for S and B separately
test_S=[]
test_B=[]
### Signal-like image indices
slike_S=[]
slike_B=[]
slike_thr=0.15

for idx,y_i in enumerate(y_valid):
    if (y_i==1):
        test_S.append(test_preds[:,1][idx])
        if (test_preds[:,1][idx]>slike_thr): slike_S.append(idx)
    elif (y_i==0):
        test_B.append(test_preds[:,1][idx])
        if (test_preds[:,1][idx]>slike_thr): slike_B.append(idx)
    else:
        print('WARNING: I found class labels different from 1 and 0?!')

plt.hist(test_S,  histtype='step', lw=2, density=True, color='black', label='S test',  bins=30)
plt.hist(test_B,  histtype='step', lw=2, density=True, color='red', label='B test',  bins=30)
plt.title('CNN output')
plt.legend(loc='upper left')
plt.show()

### Study signal-like images
print('Signal-like images!')
print('CNN cut at:\t'+str(slike_thr))
print('Signal efficiency:\t'+str(len(slike_S)/len(test_S)))
print('Background efficiency:\t'+str(len(slike_B)/len(test_B)))
print(slike_B)
