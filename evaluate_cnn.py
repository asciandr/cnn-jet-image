#!/bin/usr/python 

import sys
import subprocess
import ROOT
import helpers
from keras.models import load_model
from energyflow.utils import zero_center, standardize
import numpy as np

try:
    inputRootFiles      = str(sys.argv[1]) 
except IndexError:
    print("Argument missing.")
    print("\tUsage==> python evaluate_cnn.py inputRootFiles")
    sys.exit(1)
    pass

model = load_model('cnn_model.h5')
#model = load_model('images_20epochs_33192jets_1btag_50x50/cnn_model.h5')
#model = load_model('scaled_shifted_images_30epochs_33191jets_1btag_50x50/cnn_model.h5')
#model = load_model('NEW_NOSTANDZEROCENTER_scaled_shifted_images_20epochs_33191jets_1btag_50x50/cnn_model.h5')
#model = load_model('startFromNtuples_NEW_NOSTANDZEROCENTER_scaled_shifted_images_20epochs_27092ets_1btag_50x50/cnn_model.h5')
#model = load_model('startFromNtuples_fullMassRange_scaled_shifted_images_20epochs_1btag_50x50/cnn_model.h5')

f = ROOT.TFile(inputRootFiles)
boh=[]
empty=[]
fjs=[]
for i,e in enumerate(f.evttree):
    map_i,fji = helpers.produce_map(e,i)
    fjs.append(fji)
    if (map_i.Integral()==0):
        empty.append(i)
        continue
    boh.append(helpers.dumb_bin(map_i))

pre_images = np.asarray([x for x in boh])
images = np.expand_dims(pre_images, axis=1)
#FIXME: not needed, neither here nor in the training!
#images = standardize(*zero_center(images))
# N.B. new EF architecture requires
# inverse shape in (npix, npix, nb_chan)
# w.r.t. 2y ago!
print(images.shape)
images = np.squeeze(images, axis=1)
images = np.expand_dims(images, axis=3)
print(images.shape)

preds = model.predict(images, batch_size=20000)
cmd="cp "+inputRootFiles+" test.root"
subprocess.call(cmd, shell=True)
g = ROOT.TFile("test.root", "UPDATE")
helpers.add_cnn_branch(g, preds, empty, fjs)
