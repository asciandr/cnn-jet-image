///////////////////////// -*- C++ -*- /////////////////////////////
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

using namespace std;

float retrieve_xsec(TString xsecFile, unsigned int mcChannelNumber) {
  std::stringstream ss_mcChannelNumber; ss_mcChannelNumber << mcChannelNumber;
  std::string str_mcChannelNumber = ss_mcChannelNumber.str();
  //std::cout << "mcChannelNumber:		" << str_mcChannelNumber << std::endl;
  TString xsec_file = xsecFile; 
  std::ifstream ifs(xsec_file.Data());
  std::string aLine;
  std::vector<std::string> tokens; // Create vector to hold piecies
  if (ifs.fail()) { std::cerr << "Failure when reading xsec file: " << xsec_file << std::endl; return 0; }
  else  {
    while (getline(ifs,aLine)) {
      if ( (aLine.find("#")==0 || aLine.find(str_mcChannelNumber)==std::string::npos) ) continue;
      else {
        std::string buf; // Have a buffer string
        std::stringstream ss(aLine); // Insert the string into a stream 
        while (ss >> buf) tokens.push_back(buf);
      }
    }
  }    
  if (tokens.size()!=6) { std::cerr << "Check xsec! tokens.size()!=6, it can be buggy!!!" << std::endl; return 0; }
  //std::cout << "tokens size:	" << tokens.size() << std::endl;
  //for(unsigned int i=0; i<tokens.size(); i++) std::cout << "Split vector:		" << tokens[i] << std::endl;
  // xsec=tokens[2]; k-factor=tokens[3]; filt. eff=tokens[4]; (rel.unc.=tokens[5])
  float nom_xsec(std::stof(tokens[2])), k_fact(std::stof(tokens[3])), f_eff(std::stof(tokens[4]));
  //std::cout << "nom_xsec:	" << nom_xsec <<"	k_fact:		" << k_fact << "	f_eff:		" << f_eff << std::endl;
  float tot_xsec=nom_xsec*k_fact*f_eff; 
  return tot_xsec;

} //float retrieve_xsec
