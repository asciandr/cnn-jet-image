#!/bin/usr/python 

import os
try:
    import ROOT
except:
    print('please install ROOT in order to produce maps')
    ROOT=False
import math
import numpy as np
import array as arr
import matplotlib.pyplot as plt

useScale=True
useShift=True

minPt = 400.
maxPt = 1000.
#minMH = 105.
#maxMH = 145.
minMH = 70.
maxMH = 230.
maxEta = 2.
minJPt = 10.

def add_cnn_branch(f, preds, empty, fjs):
    tree = f.evttree
    CNN = arr.array( 'f', [-99.] )
    branch = tree.Branch("CNN", CNN, "CNN/F")
    CNN_fj_idx = arr.array( 'f', [-99.] )
    branch2 = tree.Branch("CNN_fj_idx", CNN_fj_idx, "CNN_fj_idx/F")
    count=0
    for i,e in enumerate(tree):
        CNN[0] = -99.
        if i not in empty:
            CNN[0]=preds[:,1][count]
            count=count+1

        CNN_fj_idx[0]=fjs[i]
        branch.Fill()
        branch2.Fill()
    f.Write()


def dumb_bin(map_i):
    if (map_i.Integral()==0):
        return 0
    else:
        text_file = open("dummy.txt", "w")
#        print(type(map_i))
        for i in range(1,map_i.GetXaxis().GetNbins()+1):
            check_i = i
            new_i = -99
            for j in range(1,map_i.GetYaxis().GetNbins()+1):
#                print("Bin %d %d content:\t%f",i,j,map_i.GetBinContent(i,j))
                if(new_i!=check_i and i!=0):
                    text_file.write("\n")
                text_file.write(str(map_i.GetBinContent(i,j))+" ")
                new_i = check_i
        text_file.close()
        avail=np.loadtxt("dummy.txt")
    if os.path.exists("dummy.txt"):
        os.remove("dummy.txt")
    return avail
 

def produce_map(e, i):
    histo = ROOT.TH2D(ROOT.Form("hTrackNormMap"+str(i)),"trackNormMap",50,-1.,1.,50,-1.,1.)
    isFilled = False
    FJ_idx = -99.
    for j,fj in enumerate(e.fatJetPt):
        fatJet=ROOT.TVector3(e.fatJetPx[j], e.fatJetPy[j], e.fatJetPz[j])
#        print(e.fatJetNGhostH[j])
        # N.B. in signal events ONLY one fat jet is supposed to be truth-matched to the Higgs boson
        if( fj > minPt and fj < maxPt and e.fatJetM[j] > minMH and e.fatJetM[j] < maxMH and math.fabs(fatJet.Eta()) < maxEta and (e.mcProcess!=309450 or e.fatJetNGhostH[j]==1) ):
#            print(fj)
            trackJId1 = -1
            trackJId2 = -1
            trackJPt1 = minJPt
            trackJPt2 = minJPt
            for k,tj in enumerate(e.trackJetPt):
#                print("TJ pT:\t%f",tj)
#                print("TJ FJ index:\t%d and FJ index:\t%d",e.trackJetIdFatJet[k],fj)
                if(e.trackJetIdFatJet[k]!=j): 
                    continue
                if(tj>trackJPt1):
                    trackJPt1 = tj
                    trackJId1 = k
                elif(tj>trackJPt2):
                    trackJPt2 = tj
                    trackJId2 = k
            if(trackJId1 > -1 and trackJId2 > -1 and (e.trackJetHybBEff_85[trackJId1] or e.trackJetHybBEff_85[trackJId2])):
#                print("I'm in!")
                trackJet1=ROOT.TVector3(e.trackJetPx[trackJId1], e.trackJetPy[trackJId1], e.trackJetPz[trackJId1])
                trackJet2=ROOT.TVector3(e.trackJetPx[trackJId2], e.trackJetPy[trackJId2], e.trackJetPz[trackJId2])
                if(trackJet2.Theta()-trackJet1.Theta() > 0.):
                    axisTJ=ROOT.TVector2(trackJet2.Theta()-trackJet1.Theta(), trackJet2.Phi()-trackJet1.Phi())
                else:
                    axisTJ=ROOT.TVector2(-trackJet2.Theta()+trackJet1.Theta(), -trackJet2.Phi()+trackJet1.Phi())
                phiTJ = axisTJ.Phi()
                trackJet12D=ROOT.TVector2(trackJet1.Theta()-fatJet.Theta(),trackJet1.DeltaPhi(fatJet))
                trackJet22D=ROOT.TVector2(trackJet2.Theta()-fatJet.Theta(),trackJet2.DeltaPhi(fatJet))
                trackJet12DRot=trackJet12D.Rotate(-phiTJ)
                trackJet22DRot=trackJet22D.Rotate(-phiTJ)
                scaleX = 1.
                if (useScale):
                    scaleX = math.fabs(trackJet12DRot.X()-trackJet22DRot.X())
                shiftX = 0.
                if (useShift):
                    shiftX = (trackJet12DRot.X()/scaleX-math.copysign(0.5,trackJet12DRot.X()))
                nTracks = 0
                for itk,trk in enumerate(e.trackPt):
                    track=ROOT.TVector3(e.trackPx[itk], e.trackPy[itk], e.trackPz[itk])
                    if(track.DeltaR(fatJet) < 1.0):
                        nTracks = nTracks+1
                        track2D=ROOT.TVector2(track.Theta()-fatJet.Theta(), track.DeltaPhi(fatJet))
                        track2DRot = track2D.Rotate(-phiTJ)
                        if(isFilled):
                            print("WARNING: histo was already filled for this event! histo Integral:\t%f",histo.Integral())
                            continue
                        histo.Fill(track2DRot.X()/scaleX-shiftX,track2DRot.Y(),trk/fj)
                FJ_idx = j
                isFilled = True
    return histo,FJ_idx

def plot_history(histories, key='binary_crossentropy'):
    plt.figure(figsize=(16,10))

    for name, history in histories:
        val = plt.plot(history.epoch, history.history['val_'+key],
                       '--', label=name.title()+' Val')
        plt.plot(history.epoch, history.history[key], color=val[0].get_color(),
               label=name.title()+' Train')

    plt.xlabel('Epochs')
    plt.ylabel(key.replace('_',' ').title())
    plt.legend()

    plt.xlim([0,max(history.epoch)])
    plt.show()

