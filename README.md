**N.B. as shown in the [Google Colab notebook](https://colab.research.google.com/drive/1RVji616zFhpBu9Oej5dSxXTvoK5UrFoC#scrollTo=GXQ3kdxbaUjm), step-by-step architectures overperform energy-flow ones. Original training scripts to dump track images and run the CNN training are called `ef_train_cnn.py` and `ef_my_cnn.py`**

# 1-step training (pure python) - Produce ROOT::TH2 maps from ROOT ntuples, transform them into input pixelated images and run CNN training
Run Convolutional Neural Network training for jet image recognition (through ROOT::TH2 input maps and pixelated images creation) on ntuples with full track information - the map production (training) can be run on lxplus itself after setting ROOT (through ATLAS ML singularities, see `lxplus_singularity.sh`):

```bash
python train_cnn.py
```

**N.B. the input hdf5 file will not be clobbered, if available, as far as `writeDataToH5 = False`.**

# [DEPRECATED] 2-steps training (C+python) - Produce input pixelated images from ROOT::TH2 input maps and run CNN training

If ROOT::TH2 input maps are stored in a ROOT file beforehand, create pixel inputs:

```bash
root -l dump_bin_content.C
```

and run Convolutional Neural Network training for jet image recognition:

```bash
python ef_my_cnn.py
```

# Evaluate CNN model output on skimmed ntuples

Skim input ntuple, evaluate CNN output and store it in a new branch, e.g.:

```bash
python run.py file.txt selection.txt outFolder.txt
```

# CNN feature maps

Get and check feature maps from hidden layers:

```bash
python check_feature_maps.py
```

# Run training on SWAN Jupyter & Google Colab(+GPU accelerator!) notebooks

Links:

- [SWAN Jupyter notebook](https://swan003.cern.ch/user/asciandr/notebooks/SWAN_projects/CNN_jetImages/RUN.ipynb)
- [Google Colab notebook](https://colab.research.google.com/drive/1RVji616zFhpBu9Oej5dSxXTvoK5UrFoC#scrollTo=GXQ3kdxbaUjm)
