# standard library imports
from __future__ import absolute_import, division, print_function

import os, os.path
import sys
from os import path
from keras.utils.vis_utils import plot_model

# standard numerical library imports
import numpy as np
from random import sample 
import h5py
import math

# energyflow imports
import energyflow as ef
from energyflow.archs import CNN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, pixelate, standardize, to_categorical, zero_center

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

# ROOT-related imports
import ROOT
import helpers

################################### SETTINGS ###################################

# data controls
val_frac, test_frac = 0.1, 0.15
# same number of S and B events?
balanced_data = False

# image parameters
R = 0.4
img_width = 2*R
npix = 50
nb_chan = 1
norm = True

# required network architecture parameters
input_shape = (npix, npix, nb_chan)
filter_sizes = [8, 4, 4]
#num_filters = [8, 8, 8] # very small so can run on non-GPUs in reasonable time
num_filters = [50, 50, 50] 

# optional network architecture parameters
dense_sizes = [50]
pool_sizes = 2

# network training parameters
#num_epoch = 1
#num_epoch = 2
#num_epoch = 5
#num_epoch = 10
num_epoch = 20
#num_epoch = 30

#batch_size = 10
batch_size = 1000

# regularization
conv_dropouts=0

################################################################################

# plot some data inputs to have a look?
plotData = False#True

# overwrite hdf5 file with new data?
writeDataToH5 = False#True

# check whether data is already available
# in a hdf5 file and, if yes, do not clobber it
hdf5Name = 'dataset.h5'
dataIsThere = path.exists(hdf5Name)

if (dataIsThere and not writeDataToH5):
    print('WARNING: '+hdf5Name+' is available and I am going to take images/labels from there!')
    # load dataset
    h5f = h5py.File(hdf5Name,'r')
    images = h5f['images'][:]
    Y = to_categorical(h5f['labels'][:], num_classes=2)
else:
    # load data
    S_inputRootFiles='SKIM_output/ggH.root'
    B_inputRootFiles='SKIM_output/QCD_AUGMENTED_Info_evttree.root'
    #S_inputRootFiles='SKIM_output/user.asciandr.17456028._000002.evttree.root'
    #B_inputRootFiles='SKIM_output/user.asciandr.17440819._000001.evttree.root'
    # signal ntuple
    print('Opening signal ROOT file...')
    f_sig = ROOT.TFile(S_inputRootFiles)
    # signal maps
    print('Building signal maps...')
    S_boh=[]
    for i,e in enumerate(f_sig.evttree):
        map_i,fji = helpers.produce_map(e,i)
        if (map_i.Integral()==0):
            continue
        S_boh.append(helpers.dumb_bin(map_i))
    # background ntuple
    print('Opening background ROOT file...')
    f_bkg = ROOT.TFile(B_inputRootFiles)
    # background maps
    print('Building background maps...')
    B_boh=[]
    for i,e in enumerate(f_bkg.evttree):
        map_i,fji = helpers.produce_map(e,i)
        if (map_i.Integral()==0):
            continue
        B_boh.append(helpers.dumb_bin(map_i))
    
    # balanced data size?
    S_boh_new=[]
    B_boh_new=[]
    if(balanced_data and len(S_boh)!=len(B_boh)):
        print('Unbalanced dataset and you asked me to balance it!')
        n_max = len(S_boh)
        if (len(S_boh)>len(B_boh)):
            n_max = len(B_boh)
        print('Number of S and B events:\t%d',n_max)
        S_boh_new = sample(S_boh,n_max) 
        B_boh_new = sample(B_boh,n_max) 
    else:
        S_boh_new = S_boh
        B_boh_new = B_boh
    
    # signal images
    S_pre_images = np.asarray([x for x in S_boh_new])
    # background images
    B_pre_images = np.asarray([x for x in B_boh_new])
    
    S_images = np.expand_dims(S_pre_images, axis=1)
    B_images = np.expand_dims(B_pre_images, axis=1)
    
    # concatenate S and B images
    images = np.concatenate((S_images, B_images), axis=0)
    
    # S or B?
    y = np.array([-1 for x in images])
    for idx,image in enumerate(images):
        if(idx<len(S_boh_new)):
            y[idx]=1
        else:
            y[idx]=0
    
    # convert labels to categorical
    Y = to_categorical(y, num_classes=2)
    
    # save images and labels into hdf5 file
    if (writeDataToH5):
        print('Going to save dataset in '+hdf5Name+'...')
        h5f = h5py.File(hdf5Name, 'w')
        h5f.create_dataset('images', data=images)
        h5f.create_dataset('labels', data=y)
        h5f.close()

# N.B. new EF architecture requires
# inverse shape in (npix, npix, nb_chan)
# w.r.t. 2y ago!
print(images.shape)
images = np.squeeze(images, axis=1)
images = np.expand_dims(images, axis=3)
print(images.shape)
print(Y.shape)

print('Done making/retrieving jet images')

# do train/val/test split 
(X_train, X_val, X_test,
 Y_train, Y_val, Y_test) = data_split(images, Y, val=val_frac, test=test_frac)

print('Done train/val/test split')

if (plotData):
    print('Plotting images')
    # n. of plots to be plotted
    nPlots = 2500
    for cl in range(2):
#        print('cl='+str(cl))
        fig = plt.figure(figsize=(10,10))
        myTitle = 'H->bb images' if (cl==0) else 'g->bb images'
        myLabel = 1 if (cl==0) else 0
        fig.suptitle(myTitle, fontsize=20)
        count=0
        for i in range(Y_train.shape[0]):
#            print (i)
#            print (count)
            if (count==nPlots):
                break
            if (Y_train[:,1][i]==myLabel):
                if (cl==0):
                    HbbImage = X_train[i][0]/nPlots if (count==0) else HbbImage+X_train[i][0]/nPlots
                else:
                    gbbImage = X_train[i][0]/nPlots if (count==0) else gbbImage+X_train[i][0]/nPlots
                plt.subplot(int(math.sqrt(nPlots)),int(math.sqrt(nPlots)),count+1)
                plt.xticks([])
                plt.yticks([])
                plt.grid(False)
#                print(X_train.shape)
#                print(X_train[i][0])
                plt.imshow(X_train[i][0], cmap='jet', vmin=0, vmax=0.01)
                count=count+1
        plt.show()
    # summed images
    np.set_printoptions(threshold=sys.maxsize)
    print('Hbb Image:')
    print(HbbImage)
    plt.imshow(HbbImage, cmap='jet', vmin=0, vmax=0.005)
    fig.suptitle('Summed H->bb images', fontsize=20)
    plt.show()
    print('gbb Image:')
    print(gbbImage)
    plt.imshow(gbbImage, cmap='jet', vmin=0, vmax=0.0005)
    fig.suptitle('Summed g->bb images', fontsize=20)
    plt.show()

#FIXME: not needed, neither here nor in the evaluation!
# preprocess by zero centering images and standardizing each pixel
#X_train, X_val, X_test = standardize(*zero_center(X_train, X_val, X_test))

print('Finished preprocessing')
print('Model summary:')

# build architecture
hps = {'input_shape': input_shape,
       'filter_sizes': filter_sizes,
       'num_filters': num_filters,
       'dense_sizes': dense_sizes,
       'conv_dropouts' : conv_dropouts,
       'pool_sizes': pool_sizes}
cnn = CNN(hps)

print('Built architecture')

# train model
history=cnn.fit(X_train, Y_train,
          epochs=num_epoch,
          batch_size=batch_size,
          validation_data=(X_val, Y_val),
          verbose=2)
#          verbose=1)

# save model
cnn.model.save('cnn_model.h5')
# get the architecture as a json string
arch = cnn.model.to_json()
# save the architecture string to a file somehow, the below will work
with open('architecture.json', 'w') as arch_file:
    arch_file.write(arch)
# now save the weights as an HDF5 file
cnn.model.save_weights('weights.h5')

plot_model(cnn.model, to_file='model.png', show_shapes=True, show_layer_names=True)

# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# get predictions on test & train data
test_preds = cnn.predict(X_test, batch_size=20000)
train_preds = cnn.predict(X_train, batch_size=20000)

#Plot CNN output for test & training samples
plt.hist(test_preds[:,1],  histtype='step', lw=2, density=True, color='black', label='tot test',  bins=40)
plt.hist(train_preds[:,1], histtype='step', lw=2, density=True, color='red',   label='tot train', bins=40)
plt.title('CNN output')
plt.legend(loc='upper left')
plt.show()

#Plot the same for S and B separately
test_S=[]
test_B=[]
train_S=[]
train_B=[]

for idx,y_i in enumerate(Y_train[:,1]):
    if (y_i==1):
        train_S.append(train_preds[:,1][idx])
    elif (y_i==0):
        train_B.append(train_preds[:,1][idx])
    else:
        print('WARNING: I found class labels different from 1 and 0?!')

plt.hist(train_S,  histtype='step', lw=2, density=True, color='black', label='S train',  bins=40)
plt.hist(train_B,  histtype='step', lw=2, density=True, color='red', label='B train',  bins=40)
plt.title('CNN output')
plt.legend(loc='upper left')
plt.show()

# get ROC curve if we have sklearn
if roc_curve:
    cnn_fp, cnn_tp, threshs = roc_curve(Y_test[:,1], test_preds[:,1])

    # get area under the ROC curve
    auc = roc_auc_score(Y_test[:,1], test_preds[:,1])
    print()
    print('CNN AUC:', auc)
    print()

    # make ROC curve plot if we have matplotlib
    if plt:

        # some nicer plot settings 
        plt.rcParams['figure.figsize'] = (4,4)
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['figure.autolayout'] = True

        # plot the ROC curves
        plt.plot(cnn_tp, 1-cnn_fp, '-', color='black', label='CNN')

        # axes labels
        plt.xlabel('H->bb Efficiency')
        plt.ylabel('g->bb Rejection')

        # axes limits
        plt.xlim(0, 1)
        plt.ylim(0, 1)

        # make legend and show plot
        plt.legend(loc='lower left', frameon=False)
        plt.show()
