# standard library imports
from __future__ import absolute_import, division, print_function

import os, os.path
from keras.utils import plot_model

# standard numerical library imports
import numpy as np

# energyflow imports
import energyflow as ef
from energyflow.archs import CNN
from energyflow.datasets import qg_jets
from energyflow.utils import data_split, pixelate, standardize, to_categorical, zero_center

# attempt to import sklearn
try:
    from sklearn.metrics import roc_auc_score, roc_curve
except:
    print('please install scikit-learn in order to make ROC curves')
    roc_curve = False

# attempt to import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    print('please install matploltib in order to make plots')
    plt = False

################################### SETTINGS ###################################

# data controls
num_data = len([name for name in os.listdir('pixels')])
val_frac, test_frac = 0.1, 0.15

# image parameters
R = 0.4
img_width = 2*R
npix = 50
nb_chan = 1
norm = True

# required network architecture parameters
input_shape = (nb_chan, npix, npix)
filter_sizes = [8, 4, 4]
#num_filters = [8, 8, 8] # very small so can run on non-GPUs in reasonable time
num_filters = [50, 50, 50] 

# optional network architecture parameters
dense_sizes = [50]
pool_sizes = 2

# network training parameters
#num_epoch = 2
#num_epoch = 5
#num_epoch = 10
num_epoch = 20
#num_epoch = 30
batch_size = 1000

################################################################################

# load data
X, y = qg_jets.load(num_data=num_data)

boh=[]

#boh.append(np.loadtxt('pixels/sig_pixels_0.txt'))
for idx,name in enumerate(os.listdir('pixels')):
    boh.append(np.loadtxt('pixels/'+name))
    if 'sig' in name:
        y[idx]=1
#        print(name)
    else:
        y[idx]=0
#        print(name)

# convert labels to categorical
Y = to_categorical(y, num_classes=2)

pre_images = np.asarray([x for x in boh])

images= np.expand_dims(pre_images, axis=1)

print(images.shape)
print(y.shape)

print('Done making jet images')

# do train/val/test split 
(X_train, X_val, X_test,
 Y_train, Y_val, Y_test) = data_split(images, Y, val=val_frac, test=test_frac)

print('Done train/val/test split')

#FIXME: not needed, neither here nor in the evaluation!
# preprocess by zero centering images and standardizing each pixel
#X_train, X_val, X_test = standardize(*zero_center(X_train, X_val, X_test))

print('Finished preprocessing')
print('Model summary:')

# build architecture
hps = {'input_shape': input_shape,
       'filter_sizes': filter_sizes,
       'num_filters': num_filters,
       'dense_sizes': dense_sizes,
       'pool_sizes': pool_sizes}
cnn = CNN(hps)

print('Built architecture')

# train model
history=cnn.fit(X_train, Y_train,
          epochs=num_epoch,
          batch_size=batch_size,
          validation_data=(X_val, Y_val),
          verbose=2)
#          verbose=1)

# save model
cnn.model.save('cnn_model.h5')
# get the architecture as a json string
arch = cnn.model.to_json()
# save the architecture string to a file somehow, the below will work
with open('architecture.json', 'w') as arch_file:
    arch_file.write(arch)
# now save the weights as an HDF5 file
cnn.model.save_weights('weights.h5')

plot_model(cnn.model, to_file='model.png', show_shapes=True, show_layer_names=True)

# Plot training & validation accuracy values
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# get predictions on test & train data
test_preds = cnn.predict(X_test, batch_size=20000)
train_preds = cnn.predict(X_train, batch_size=20000)

#Plot CNN output for test & training samples
plt.hist(test_preds[:,1],  histtype='step', lw=2, normed=True, color='black', label='tot test',  bins=40)
plt.hist(train_preds[:,1], histtype='step', lw=2, normed=True, color='red',   label='tot train', bins=40)
plt.title('CNN output')
plt.legend(loc='upper left')
plt.show()

#Plot the same for S and B separately
test_S=[]
test_B=[]
train_S=[]
train_B=[]

for idx,y_i in enumerate(Y_train[:,1]):
    if (y_i==1):
        train_S.append(train_preds[:,1][idx])
    elif (y_i==0):
        train_B.append(train_preds[:,1][idx])
    else:
        print("WARNING: I found class labels different from 1 and 0?!")

plt.hist(train_S,  histtype='step', lw=2, normed=True, color='black', label='S train',  bins=40)
plt.hist(train_B,  histtype='step', lw=2, normed=True, color='red', label='B train',  bins=40)
plt.title('CNN output')
plt.legend(loc='upper left')
plt.show()

# get ROC curve if we have sklearn
if roc_curve:
    cnn_fp, cnn_tp, threshs = roc_curve(Y_test[:,1], test_preds[:,1])

    # get area under the ROC curve
    auc = roc_auc_score(Y_test[:,1], test_preds[:,1])
    print()
    print('CNN AUC:', auc)
    print()

    # make ROC curve plot if we have matplotlib
    if plt:

        # some nicer plot settings 
        plt.rcParams['figure.figsize'] = (4,4)
        plt.rcParams['font.family'] = 'serif'
        plt.rcParams['figure.autolayout'] = True

        # plot the ROC curves
        plt.plot(cnn_tp, 1-cnn_fp, '-', color='black', label='CNN')

        # axes labels
        plt.xlabel('H->bb Efficiency')
        plt.ylabel('g->bb Rejection')

        # axes limits
        plt.xlim(0, 1)
        plt.ylim(0, 1)

        # make legend and show plot
        plt.legend(loc='lower left', frameon=False)
        plt.show()
