from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from tensorflow.keras import datasets, layers, models
from keras.layers import LeakyReLU
from keras.utils.vis_utils import plot_model
from sklearn.model_selection import train_test_split

from os import path
import sys
import numpy as np
import h5py
import ROOT
import helpers

import matplotlib.pyplot as plt

# check whether data is already available
# in a hdf5 file and, if yes, load input data
hdf5Name = 'dataset.h5'
dataIsThere = path.exists(hdf5Name)

if (dataIsThere):
    print('WARNING: '+hdf5Name+' is available and I am going to take images/labels from there!')
    # load dataset
    h5f = h5py.File(hdf5Name,'r')
    images = h5f['images'][:]
    y = h5f['labels'][:]
else:
    print('Did not find input data. Exiting...')
    sys.exit(1)

# data stored for EF-like CNN have 
# shape (Nev, 1, Npix, Npix) need to 
# switch to (Nev, Npix, Npix, 1)
print(images.shape)
images = np.squeeze(images, axis=1)
images = np.expand_dims(images, axis=3)
print(images.shape)
print(y.shape)

model = models.Sequential()
#model.add(layers.Conv2D(5, (5, 5), activation='relu', input_shape=(50, 50, 1)))
#model.add(layers.Conv2D(64, (5, 5), input_shape=(50, 50, 1)))
#model.add(layers.LeakyReLU(alpha=0.01))
#model.add(layers.MaxPooling2D((2, 2)))
#model.add(layers.Conv2D(64, (3, 3), activation='relu'))
#model.add(layers.MaxPooling2D((2, 2)))
#model.add(layers.Conv2D(64, (3, 3), activation='relu'))
#model.add(layers.Flatten())
# do not need to input shape
# for flattening when starting 
# with convolution+pooling steps
model.add(layers.Flatten(input_shape=(50, 50, 1)))
model.add(layers.Dense(16, activation='relu'))
#model.add(layers.Dense(32))
#model.add(layers.LeakyReLU(alpha=0.01))
model.add(layers.Dense(2, activation='softmax'))

model.summary()
plot_model(model, to_file='Basic_outputs/Basic_model.png', show_shapes=True, show_layer_names=True)

model.compile(optimizer='adam',
#model.compile(optimizer='sgd',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# split into train, valid, test
x_train, x_valid, y_train, y_valid = train_test_split(images, y, test_size=0.30, shuffle= True)
history = model.fit(x_train, y_train, 
                    validation_data=(x_valid,y_valid),
                    batch_size=512,
                    epochs=30,
                    verbose=2)

model.save_weights('Basic_outputs/Basic_weights.h5')

helpers.plot_history([('baseline', history)],'loss')

valid_loss, valid_acc = model.evaluate(x_valid,y_valid)
print(valid_acc)
