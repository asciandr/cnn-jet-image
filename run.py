#!/bin/usr/python 

import ROOT
from ROOT import gROOT
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import glob
import subprocess
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

inputRootFiles	= ""
selection 	= ""
outFolder 	= ""

try:
    inputRootFiles	= str(sys.argv[1])
    selection 		= str(sys.argv[2])
    outFolder 		= str(sys.argv[3])
except IndexError:
    print("Argument missing.")
    print("\tUsage==> python run.py inputRootFiles selection outFolder")
    sys.exit(1)
    pass

if not os.path.exists("./"+inputRootFiles) and not os.path.exists(inputRootFiles): 
    print("Input "+str(sys.argv[1])+" not found, aborting...")
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print("Input "+str(sys.argv[2])+" not found, aborting...")
    sys.exit(1)
    pass
if not os.path.exists("./"+outFolder):
    print("Input "+str(sys.argv[3])+" not found, aborting...")
    sys.exit(1)
    pass

print("Compiling macros...")
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L skimNtuples.C+")
if cmpFailure:
    print("Compilation failure, aborting...")
    sys.exit(1)
    pass

# Skim ntuple
outFile_names = ROOT.skimNtuples( inputRootFiles, selection, outFolder)
# Evaluate CNN output on latest output
outFile = outFile_names[0]
print("Running evaluate_cnn.py on "+outFile)
cmd="python evaluate_cnn.py "+outFile
subprocess.call(cmd, shell=True)
